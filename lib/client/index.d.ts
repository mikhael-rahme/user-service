import { ServiceResult, ServiceRequest } from 'toolkit';
import { GetUserRequest, CreateUserRequest, ListUsersRequest, ListUsersResponse, DeleteUserRequest, DeleteUserResponse, UpdateUserRequest, UpdateUserResponse, ListGroupsRequest, ListGroupsResponse, CreateGroupRequest, DeleteGroupRequest, DeleteGroupResponse, AddUserToGroupRequest, AddUserToGroupResponse, RemoveUserFromGroupRequest, RemoveUserFromGroupResponse, ListUsersInGroupRequest, ListUsersInGroupResponse, ChangeUserPasswordRequest, ChangeUserPasswordResponse } from '../Types';
declare const createClient: (caller: string, stage: string, options?: {
    serviceRequest?: ServiceRequest;
    version?: string;
}) => {
    getUser: (request: GetUserRequest) => Promise<ServiceResult<{
        username: string;
        userAttributes: {
            name: string;
            value: string;
        }[];
        status: string;
        enabled: boolean;
        groups: {
            groupName: string;
            description?: string;
        }[];
    }>>;
    listUsers: (request: ListUsersRequest) => Promise<ServiceResult<ListUsersResponse>>;
    createUser: (request: CreateUserRequest) => Promise<ServiceResult<{
        username: string;
        userAttributes: {
            name: string;
            value: string;
        }[];
        status: string;
        enabled: boolean;
        groups: {
            groupName: string;
            description?: string;
        }[];
    }>>;
    deleteUser: (request: DeleteUserRequest) => Promise<ServiceResult<DeleteUserResponse>>;
    updateUser: (request: UpdateUserRequest) => Promise<ServiceResult<UpdateUserResponse>>;
    listGroups: (request: ListGroupsRequest) => Promise<ServiceResult<ListGroupsResponse>>;
    createGroup: (request: CreateGroupRequest) => Promise<ServiceResult<{
        groupName: string;
        description?: string;
    }>>;
    deleteGroup: (request: DeleteGroupRequest) => Promise<ServiceResult<DeleteGroupResponse>>;
    addUserToGroup: (request: AddUserToGroupRequest) => Promise<ServiceResult<AddUserToGroupResponse>>;
    removeUserFromGroup: (request: RemoveUserFromGroupRequest) => Promise<ServiceResult<RemoveUserFromGroupResponse>>;
    listUsersInGroup: (request: ListUsersInGroupRequest) => Promise<ServiceResult<ListUsersInGroupResponse>>;
    changeUserPassword: (request: ChangeUserPasswordRequest) => Promise<ServiceResult<ChangeUserPasswordResponse>>;
};
export default createClient;
