"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const toolkit_1 = require("toolkit");
const aws_sdk_1 = require("aws-sdk");
const { name: serviceName, version: serviceVersion, } = require('../../package.json');
const { createLambdaRequest, } = toolkit_1.LambdaUtils;
const createClient = (caller, stage, options = {}) => {
    const { serviceRequest, version, } = options;
    const lambda = new aws_sdk_1.Lambda({
        apiVersion: '2015-03-31',
        region: process.env.AWS_REGION || 'us-east-1',
    });
    const createLambdaRequestArguments = (functionName) => ({
        lambda,
        stage,
        serviceName,
        serviceRequest,
        caller,
        functionName,
    });
    const createLambdaRequestOptions = (request) => ({
        serviceRequest,
        version: (version || serviceVersion).replace(/\./g, '-'),
        payload: request,
    });
    return {
        getUser: (request) => createLambdaRequest(createLambdaRequestArguments('getUser'), createLambdaRequestOptions(request)),
        listUsers: (request) => createLambdaRequest(createLambdaRequestArguments('listUsers'), createLambdaRequestOptions(request)),
        createUser: (request) => createLambdaRequest(createLambdaRequestArguments('createUser'), createLambdaRequestOptions(request)),
        deleteUser: (request) => createLambdaRequest(createLambdaRequestArguments('deleteUser'), createLambdaRequestOptions(request)),
        updateUser: (request) => createLambdaRequest(createLambdaRequestArguments('updateUser'), createLambdaRequestOptions(request)),
        listGroups: (request) => createLambdaRequest(createLambdaRequestArguments('listGroups'), createLambdaRequestOptions(request)),
        createGroup: (request) => createLambdaRequest(createLambdaRequestArguments('createGroup'), createLambdaRequestOptions(request)),
        deleteGroup: (request) => createLambdaRequest(createLambdaRequestArguments('deleteGroup'), createLambdaRequestOptions(request)),
        addUserToGroup: (request) => createLambdaRequest(createLambdaRequestArguments('addUserToGroup'), createLambdaRequestOptions(request)),
        removeUserFromGroup: (request) => createLambdaRequest(createLambdaRequestArguments('removeUserFromGroup'), createLambdaRequestOptions(request)),
        listUsersInGroup: (request) => createLambdaRequest(createLambdaRequestArguments('listUsersInGroup'), createLambdaRequestOptions(request)),
        changeUserPassword: (request) => createLambdaRequest(createLambdaRequestArguments('changeUserPassword'), createLambdaRequestOptions(request)),
    };
};
exports.default = createClient;
