import { CreateCustomerTestObject } from 'customer-service';
import { UserServiceTestObject } from './Types';
/**
 * TODO: Add customEnvironment injection such that graphql service may addon a createUser
 * @param testInjects An optional parameter, which replaces any portion of a test object with the given value.
 *  -- Usecase: injecting before01Integration into testSuite
 */
declare const createTestObject: (testInjects?: any, customEnvironment?: CreateCustomerTestObject) => UserServiceTestObject;
/**
 * Works as a framework for all tests within user-service.
 * Creates and removes an environment before and after tests respectively.
 * @param userServiceTestObject
 */
declare const testSuite: (userServiceTestObject: UserServiceTestObject) => void;
export { testSuite, createTestObject };
