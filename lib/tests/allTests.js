"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const createCustomerTests = require("customer-service/lib/lambdas/createCustomer/createCustomerTests");
const utils_1 = require("../utils");
const _1 = require("./");
const before00DefineEnv = (testObject) => __awaiter(this, void 0, void 0, function* () {
    return testObject ? testObject : createCustomerTests.createTestObject();
});
const before01EnvSetup = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield createCustomerTests.before00Setup(testObject);
    yield createCustomerTests.before01Run(testObject);
    yield createCustomerTests.before02CollectResults(testObject);
    return testObject;
});
const after00TearDownEnv = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        yield createCustomerTests.after00TearDown(testObject);
    }
    catch (err) {
        console.log(`Error during allTests.test.ts teardown for UserPool ${testObject.createCustomerResponse.customerId}:\n\n ${err}`);
    }
    return testObject;
});
/**
 * TODO: Add customEnvironment injection such that graphql service may addon a createUser
 * @param testInjects An optional parameter, which replaces any portion of a test object with the given value.
 *  -- Usecase: injecting before01Integration into testSuite
 */
const createTestObject = (testInjects = {}, customEnvironment) => {
    const injectOrDefault = (testKey, functionName) => testInjects[testKey] && testInjects[testKey][functionName] ?
        testInjects[testKey][functionName] : _1.allTests[testKey][functionName];
    return {
        environment: null,
        tests: Object.keys(_1.allTests)
            .map(testKey => ({
            before00Setup: injectOrDefault(testKey, 'before00Setup'),
            before01Run: injectOrDefault(testKey, 'before01Run'),
            before02CollectResults: injectOrDefault(testKey, 'before02CollectResults'),
            testObject: injectOrDefault(testKey, 'createTestObject')(),
            after: injectOrDefault(testKey, 'after00TearDown'),
            tests: injectOrDefault(testKey, 'test00'),
            testName: testKey,
        })),
    };
};
exports.createTestObject = createTestObject;
/**
 * Works as a framework for all tests within user-service.
 * Creates and removes an environment before and after tests respectively.
 * @param userServiceTestObject
 */
const testSuite = (userServiceTestObject) => {
    describe(`user-service:`, () => {
        before(() => __awaiter(this, void 0, void 0, function* () {
            const environment = yield before00DefineEnv().then(before01EnvSetup);
            utils_1.findDeepAndAssign(userServiceTestObject, 'environment', environment);
            console.log(`UserPoolId: ${environment.createCustomerResponse.customerId}`);
        }));
        userServiceTestObject.tests.forEach((test) => __awaiter(this, void 0, void 0, function* () {
            describe(`${test.testName.split('Tests')[0]}:`, () => {
                before(() => __awaiter(this, void 0, void 0, function* () {
                    yield test.before00Setup(test.testObject)
                        .then(test.before01Run)
                        .then(test.before02CollectResults);
                }));
                test.tests(test.testObject);
                after(() => __awaiter(this, void 0, void 0, function* () {
                    try {
                        yield test.after(test.testObject);
                    }
                    catch (err) {
                        test.testObject.error = err;
                    }
                }));
            });
        }));
        after(() => __awaiter(this, void 0, void 0, function* () {
            yield after00TearDownEnv(userServiceTestObject.environment);
        }));
    });
};
exports.testSuite = testSuite;
