import { CreateCustomerResponse } from 'customer-service';
export declare type Customer = {
    customerId: string;
    clientId?: string;
    customerName?: string;
};
export interface UserServiceTestObject {
    environment: CreateCustomerResponse | Customer;
    customEnvironment?: CreateCustomerResponse;
    tests: {
        before00Setup: any;
        before01Run: any;
        before02CollectResults: any;
        testObject: any;
        after: any;
        tests: any;
        testName: string;
    }[];
}
