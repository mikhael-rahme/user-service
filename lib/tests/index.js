"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const addUserToGroupTests = require("../lambdas/addUserToGroup/addUserToGroupTests");
const createGroupTests = require("../lambdas/createGroup/createGroupTests");
const createUserTests = require("../lambdas/createUser/createUserTests");
const deleteGroupTests = require("../lambdas/deleteGroup/deleteGroupTests");
const deleteUserTests = require("../lambdas/deleteUser/deleteUserTests");
const getUserTests = require("../lambdas/getUser/getUserTests");
const listGroupsTests = require("../lambdas/listGroups/listGroupsTests");
const listUsersTests = require("../lambdas/listUsers/listUsersTests");
const listUsersInGroupTests = require("../lambdas/listUsersInGroup/listUsersInGroupTests");
const removeUserFromGroupTests = require("../lambdas/removeUserFromGroup/removeUserFromGroupTests");
// import * as resetUserPasswordTests from '../lambdas/resetUserPassword/resetUserPasswordTests';
const updateUserTests = require("../lambdas/updateUser/updateUserTests");
// import * as forgotPasswordTests from './lambdas/forgotPassword/forgotPasswordTests';
const allTests_1 = require("./allTests");
exports.testSuite = allTests_1.testSuite;
exports.createTestObject = allTests_1.createTestObject;
const changeUserPasswordTests = require("../lambdas/changeUserPassword/changeUserPasswordTests");
exports.allTests = {
    addUserToGroupTests,
    createGroupTests,
    createUserTests,
    deleteGroupTests,
    deleteUserTests,
    getUserTests,
    listGroupsTests,
    listUsersTests,
    listUsersInGroupTests,
    removeUserFromGroupTests,
    // resetUserPasswordTests,
    updateUserTests,
    // forgotPasswordTests,
    changeUserPasswordTests,
};
