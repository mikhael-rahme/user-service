import { AuthenticationResponse } from '../../Types';
export declare type ReauthorizeRequest = {
    customerId: string;
    clientId: string;
    username: string;
    refreshToken: string;
};
export declare type ReauthorizeResponse = {
    authenticationResponse: AuthenticationResponse;
};
