"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
/*
  Used to refresh an accessToken, if it happens to expire.
 */
const reauthorize = (request) => __awaiter(this, void 0, void 0, function* () {
    const client = new aws_sdk_1.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18',
        region: process.env.region || 'us-east-1',
    });
    const initiateAuthRequest = {
        UserPoolId: request.customerId,
        ClientId: request.clientId,
        AuthFlow: 'REFRESH_TOKEN',
        AuthParameters: { USERNAME: request.username, REFRESH_TOKEN: request.refreshToken },
    };
    const cognitoResponse = yield client.adminInitiateAuth(initiateAuthRequest).promise();
    if (cognitoResponse.ChallengeName !== undefined) {
        throw new Error('RefreshToken invalid.');
    }
    return {
        authenticationResponse: {
            accessToken: cognitoResponse.AuthenticationResult.AccessToken,
            refreshToken: cognitoResponse.AuthenticationResult.RefreshToken,
            expiresIn: cognitoResponse.AuthenticationResult.ExpiresIn,
            tokenType: cognitoResponse.AuthenticationResult.TokenType,
            idToken: cognitoResponse.AuthenticationResult.IdToken,
        },
    };
});
exports.default = reauthorize;
