import { ReauthorizeRequest, ReauthorizeResponse } from './Types';
declare const reauthorize: (request: ReauthorizeRequest) => Promise<ReauthorizeResponse>;
export default reauthorize;
