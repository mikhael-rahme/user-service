import { AuthenticationResponse } from '../../Types';
export declare type ResponseToNewPasswordRequiredChallengeType = 'ADMIN_NO_SRP_AUTH';
export declare type RespondToNewPasswordRequiredRequest = {
    customerId: string;
    clientId: string;
    username: string;
    newPassword: string;
    session: string;
};
export declare type RespondToNewPasswordRequiredResponse = {
    authenticationResponse?: AuthenticationResponse;
    challengeName?: ResponseToNewPasswordRequiredChallengeType;
    session?: string;
};
