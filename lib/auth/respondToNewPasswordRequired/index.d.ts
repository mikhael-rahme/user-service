import { RespondToNewPasswordRequiredRequest, RespondToNewPasswordRequiredResponse } from './Types';
declare const respondToNewPasswordRequired: (request: RespondToNewPasswordRequiredRequest) => Promise<RespondToNewPasswordRequiredResponse>;
export default respondToNewPasswordRequired;
