import { AuthenticationResponse } from '../../Types';
export declare type AuthorizeUserChallengeType = 'NEW_PASSWORD_REQUIRED';
export declare type AuthorizeUserRequest = {
    customerId: string;
    clientId: string;
    username: string;
    password: string;
};
export declare type AuthorizeUserResponse = {
    authenticationResponse?: AuthenticationResponse;
    challengeName?: AuthorizeUserChallengeType;
    session?: string;
};
