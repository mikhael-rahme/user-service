import { AuthorizeUserRequest, AuthorizeUserResponse } from './Types';
declare const authorizeUser: (request: AuthorizeUserRequest) => Promise<AuthorizeUserResponse>;
export default authorizeUser;
