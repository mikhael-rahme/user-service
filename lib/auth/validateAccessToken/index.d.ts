import { ValidateAccessTokenRequest } from './Types';
declare const validateAccessToken: (request: ValidateAccessTokenRequest) => Promise<boolean>;
export default validateAccessToken;
