export declare type ValidateAccessTokenRequest = {
    accessToken: string;
    issuer: string;
};
export declare type ValidateAccessTokenResponse = boolean;
