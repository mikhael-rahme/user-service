"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require('jsonwebtoken');
const jwkToPem = require('jwk-to-pem');
const Request = require('request');
// Thanks! https://aws.amazon.com/blogs/mobile/integrating-amazon-cognito-user-pools-with-api-gateway/
const validateAccessToken = (request) => __awaiter(this, void 0, void 0, function* () {
    let pems;
    const fetchJwtAndValidate = () => new Promise((res, rej) => {
        Request({ url: request.issuer + '/.well-known/jwks.json', json: true }, (error, response, body) => {
            if (!error && response.statusCode === 200) {
                pems = {};
                const keys = body['keys'];
                keys.forEach((key) => {
                    const keyId = key.kid;
                    const modulus = key.n;
                    const exponent = key.e;
                    const keyType = key.kty;
                    const jwk = { kty: keyType, n: modulus, e: exponent };
                    const pem = jwkToPem(jwk);
                    pems[keyId] = pem;
                });
                res(validateToken(pems, request));
            }
            else {
                rej(new Error(`Failed to retrieve JWKs: ${error ? error : `statusCode: ${response.statusCode}`}`));
            }
        });
    });
    if (!pems) {
        return yield fetchJwtAndValidate();
    }
    return yield validateToken(pems, request);
});
const validateToken = (pems, request) => new Promise((res, rej) => {
    const { accessToken, issuer, } = request;
    // Fail if the token is not jwt
    const decodedJwt = jwt.decode(accessToken, { complete: true });
    if (!decodedJwt) {
        rej(new Error('Not a valid JWT token!'));
    }
    // Fail if token is not from your User Pool
    if (decodedJwt.payload.iss !== issuer) {
        rej(new Error(`Invalid token issuer`));
    }
    // Reject the jwt if it's not an 'Access Token'
    if (decodedJwt.payload.token_use !== 'access') {
        rej(new Error(`Not an access token`));
    }
    // Get the kid from the token and retrieve corresponding PEM
    const kid = decodedJwt.header.kid;
    const pem = pems[kid];
    if (!pem) {
        rej(new Error('Invalid accessToken!'));
    }
    // Verify the signature of the JWT token to ensure it's really coming from your User Pool
    jwt.verify(accessToken, pem, { issuer }, (err, payload) => {
        res(!err);
    });
});
exports.default = validateAccessToken;
