import { CognitoIdentityServiceProvider as Cog } from 'aws-sdk';
import { CreateCustomerTestObject } from 'customer-service';
export * from './lambdas/addUserToGroup/Types';
export * from './lambdas/createGroup/Types';
export * from './lambdas/createUser/Types';
export * from './lambdas/deleteGroup/Types';
export * from './lambdas/deleteUser/Types';
export * from './lambdas/getUser/Types';
export * from './lambdas/listGroups/Types';
export * from './lambdas/listUsers/Types';
export * from './lambdas/listUsersInGroup/Types';
export * from './auth/reauthorize/Types';
export * from './lambdas/removeUserFromGroup/Types';
export * from './lambdas/resetUserPassword/Types';
export * from './lambdas/updateUser/Types';
export * from './lambdas/forgotPassword/Types';
export * from './lambdas/changeUserPassword/Types';
export * from './auth/authorizeUser/Types';
export * from './auth/respondToNewPasswordRequired/Types';
export * from './tests/Types';
export interface UserServiceLambdaTestObject {
    environment: CreateCustomerTestObject;
}
export declare type UserAttribute = {
    name: string;
    value: string;
};
export declare type Group = {
    groupName: string;
    description?: string;
};
export declare type User = {
    username: string;
    userAttributes: UserAttribute[];
    status: Cog.Types.UserStatusType;
    enabled: boolean;
    groups: Group[];
};
export declare type IterrexUser = {
    username: string;
    customerId: string;
    customerName: string;
    clientId: string;
    groupNames: string[];
};
export declare type AuthenticationResponse = {
    accessToken: string;
    refreshToken: string;
    expiresIn: number;
    tokenType: string;
    idToken: string;
};
export declare const cognitoUserAttributeToAttribute: (att: any) => {
    name: any;
    value: any;
};
export declare const cognitoUserToUser: (user: any) => {
    username: any;
    status: any;
    enabled: any;
    groups: any;
    userAttributes: any;
};
export declare const cognitoAuthResToAuthRes: (auth: Cog.AuthenticationResultType) => {
    accessToken: string;
    refreshToken: string;
    expiresIn: number;
    tokenType: string;
    idToken: string;
};
export declare const cognitoGroupToGroup: (grp: Cog.GroupType) => {
    groupName: string;
    description: string;
};
