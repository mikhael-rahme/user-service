import { ServiceResult } from 'toolkit';
export declare const nnnu: (value: any) => boolean;
export declare const findDeepAndReplaceAllPropertyValue: (object: any, propertyName: string, value: any) => any;
export declare const findDeepAndAssign: (object: any, propertyName: string, value: any) => any;
export declare const client: {
    getUser: (request: {
        customerId: string;
        username: string;
    }) => Promise<ServiceResult<{
        username: string;
        userAttributes: {
            name: string;
            value: string;
        }[];
        status: string;
        enabled: boolean;
        groups: {
            groupName: string;
            description?: string;
        }[];
    }>>;
    listUsers: (request: {
        customerId: string;
        limit?: number;
        paginationToken?: string;
        attributeFilters?: {
            name: string;
            value: string;
        }[];
    }) => Promise<ServiceResult<{
        users: {
            username: string;
            userAttributes: {
                name: string;
                value: string;
            }[];
            status: string;
            enabled: boolean;
            groups: {
                groupName: string;
                description?: string;
            }[];
        }[];
        paginationToken?: string;
    }>>;
    createUser: (request: {
        customerId: string;
        clientId: string;
        username: string;
        password: string;
        groups?: {
            groupName: string;
            description?: string;
        }[];
        userAttributes?: {
            name: string;
            value: string;
        }[];
    }) => Promise<ServiceResult<{
        username: string;
        userAttributes: {
            name: string;
            value: string;
        }[];
        status: string;
        enabled: boolean;
        groups: {
            groupName: string;
            description?: string;
        }[];
    }>>;
    deleteUser: (request: {
        customerId: string;
        username: string;
    }) => Promise<ServiceResult<{
        username: string;
    }>>;
    updateUser: (request: {
        username: string;
        customerId: string;
        enabled?: boolean;
        groups?: {
            groupName: string;
            description?: string;
        }[];
        userAttributes?: {
            name: string;
            value: string;
        }[];
    }) => Promise<ServiceResult<{
        username: String;
        enabled?: boolean;
        groups?: {
            groupName: string;
            description?: string;
        }[];
        userAttributes?: {
            name: string;
            value: string;
        }[];
    }>>;
    listGroups: (request: {
        customerId: string;
        limit?: number;
        paginationToken?: string;
    }) => Promise<ServiceResult<{
        groups: {
            groupName: string;
            description?: string;
        }[];
        paginationToken?: string;
    }>>;
    createGroup: (request: {
        customerId: string;
        groupName: string;
        description?: string;
        precedence?: number;
        roleArn?: string;
    }) => Promise<ServiceResult<{
        groupName: string;
        description?: string;
    }>>;
    deleteGroup: (request: {
        customerId: string;
        groupName: string;
    }) => Promise<ServiceResult<{
        groupName: string;
    }>>;
    addUserToGroup: (request: {
        customerId: string;
        username: string;
        groupName: string;
    }) => Promise<ServiceResult<{
        username: string;
        groupName: string;
    }>>;
    removeUserFromGroup: (request: {
        customerId: string;
        username: string;
        groupName: string;
    }) => Promise<ServiceResult<{
        username: string;
        groupName: string;
    }>>;
    listUsersInGroup: (request: {
        customerId: string;
        groupName: string;
        limit?: number;
        paginationToken?: string;
    }) => Promise<ServiceResult<{
        users: {
            username: string;
            userAttributes: {
                name: string;
                value: string;
            }[];
            status: string;
            enabled: boolean;
            groups: {
                groupName: string;
                description?: string;
            }[];
        }[];
        paginationToken?: string;
    }>>;
    changeUserPassword: (request: {
        accessToken: string;
        oldPassword: string;
        newPassword: string;
    }) => Promise<ServiceResult<{
        error?: string;
        message?: string;
    }>>;
};
