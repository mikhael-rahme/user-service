"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const toolkit_1 = require("toolkit");
const client_1 = require("./client");
// not null, not undefined
exports.nnnu = (value) => value !== null && value !== undefined;
exports.findDeepAndReplaceAllPropertyValue = (object, propertyName, value) => {
    if (typeof object === 'object' && object !== null) {
        if (Array.isArray(object)) {
            return object.map(item => exports.findDeepAndReplaceAllPropertyValue(item, propertyName, value));
        }
        const newObject = {};
        Object.keys(object).forEach((objectKey) => {
            if (objectKey === propertyName) {
                newObject[objectKey] = value;
            }
            else {
                newObject[objectKey] = exports.findDeepAndReplaceAllPropertyValue(object[objectKey], propertyName, value);
            }
        });
        return newObject;
    }
    return object;
};
exports.findDeepAndAssign = (object, propertyName, value) => {
    if (typeof object === 'object' && object !== null) {
        if (Array.isArray(object)) {
            return object.forEach(item => exports.findDeepAndAssign(item, propertyName, value));
        }
        Object.keys(object).forEach((objectKey) => {
            if (objectKey === propertyName) {
                object[objectKey] = value;
            }
            else {
                object[objectKey] = exports.findDeepAndAssign(object[objectKey], propertyName, value);
            }
        });
    }
    return object;
};
exports.client = client_1.default('userServiceIntegrationTests', toolkit_1.testUtils.getTestStage());
