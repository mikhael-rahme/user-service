"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.cognitoUserAttributeToAttribute = (att) => ({ name: att.Name, value: att.Value });
exports.cognitoUserToUser = (user) => {
    const response = {
        username: user.Username,
        status: user.UserStatus,
        enabled: user.Enabled,
        groups: user.Groups ? user.Groups.map(exports.cognitoGroupToGroup) : [],
        userAttributes: user.UserAttributes ? user.UserAttributes.map(exports.cognitoUserAttributeToAttribute) : [],
    };
    if (response.userAttributes.length === 0) {
        response.userAttributes = user.Attributes ? user.Attributes.map(exports.cognitoUserAttributeToAttribute) : [];
    }
    return response;
};
exports.cognitoAuthResToAuthRes = (auth) => ({
    accessToken: auth.AccessToken,
    refreshToken: auth.RefreshToken,
    expiresIn: auth.ExpiresIn,
    tokenType: auth.TokenType,
    idToken: auth.IdToken,
});
exports.cognitoGroupToGroup = (grp) => ({
    groupName: grp.GroupName,
    description: grp.Description,
});
