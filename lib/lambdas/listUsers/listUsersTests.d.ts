import { ListUsersTestObject } from '../../Types';
declare const createTestObject: () => ListUsersTestObject;
declare const before00Setup: (testObject: ListUsersTestObject) => Promise<ListUsersTestObject>;
declare const before01Run: (testObject: ListUsersTestObject) => Promise<ListUsersTestObject>;
declare const run00Integration: (testObject: ListUsersTestObject) => Promise<ListUsersTestObject>;
declare const before02CollectResults: (testObject: ListUsersTestObject) => Promise<ListUsersTestObject>;
declare const after00TearDown: (testObject: ListUsersTestObject) => Promise<ListUsersTestObject>;
declare const test00: (testObject: ListUsersTestObject) => ListUsersTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
