import { ListUsersRequest, ListUsersResponse } from './Types';
declare const listUsers: (request: ListUsersRequest) => Promise<ListUsersResponse>;
export default listUsers;
