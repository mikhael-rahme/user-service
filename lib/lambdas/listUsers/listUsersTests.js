"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const toolkit_1 = require("toolkit");
const _1 = require("./");
const CreateUserTests = require("../createUser/createUserTests");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createUserTestObject: CreateUserTests.createTestObject(),
    listUsersRequest: null,
    listUsersResponse: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield CreateUserTests.before00Setup(testObject.createUserTestObject);
            yield CreateUserTests.before01Run(testObject.createUserTestObject);
            yield CreateUserTests.before02CollectResults(testObject.createUserTestObject);
            testObject.listUsersRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.listUsersResponse = yield _1.default(testObject.listUsersRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.listUsers(testObject.listUsersRequest);
        testObject.listUsersResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    // nada
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield CreateUserTests.after00TearDown(testObject.createUserTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('listUsers', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('listUsersResponse to include createUserResponse object', () => {
            const o0 = {
                username: testObject
                    .createUserTestObject
                    .createUserResponse.username,
                status: testObject
                    .createUserTestObject
                    .createUserResponse.status,
                enabled: testObject
                    .createUserTestObject
                    .createUserResponse.enabled,
            };
            const o1s = testObject.listUsersResponse.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
    });
    return testObject;
};
exports.test00 = test00;
