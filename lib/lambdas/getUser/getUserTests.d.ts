import { GetUserTestObject } from '../../Types';
declare const createTestObject: () => GetUserTestObject;
declare const before00Setup: (testObject: GetUserTestObject) => Promise<GetUserTestObject>;
declare const before01Run: (testObject: GetUserTestObject) => Promise<GetUserTestObject>;
declare const run00Integration: (testObject: GetUserTestObject) => Promise<GetUserTestObject>;
declare const before02CollectResults: (testObject: GetUserTestObject) => Promise<GetUserTestObject>;
declare const after00TearDown: (testObject: GetUserTestObject) => Promise<GetUserTestObject>;
declare const test00: (testObject: GetUserTestObject) => GetUserTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
