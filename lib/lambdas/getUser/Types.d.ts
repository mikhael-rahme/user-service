import { User, CreateUserTestObject, UserServiceLambdaTestObject } from '../../Types';
export declare type GetUserRequest = {
    customerId: string;
    username: string;
};
export declare type GetUserResponse = User;
export interface GetUserTestObject extends UserServiceLambdaTestObject {
    createUserTestObject: CreateUserTestObject;
    getUserRequest: GetUserRequest;
    getUserResponse: GetUserResponse;
    error: Error;
}
