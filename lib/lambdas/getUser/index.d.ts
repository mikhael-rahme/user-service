import { GetUserRequest } from './Types';
declare const getUser: (request: GetUserRequest) => Promise<{
    username: string;
    userAttributes: {
        name: string;
        value: string;
    }[];
    status: string;
    enabled: boolean;
    groups: {
        groupName: string;
        description?: string;
    }[];
}>;
export default getUser;
