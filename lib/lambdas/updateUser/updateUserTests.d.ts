import { UpdateUserTestObject } from '../../Types';
declare const createTestObject: () => UpdateUserTestObject;
declare const before00Setup: (testObject: UpdateUserTestObject) => Promise<UpdateUserTestObject>;
declare const before01Run: (testObject: UpdateUserTestObject) => Promise<UpdateUserTestObject>;
declare const run00Integration: (testObject: UpdateUserTestObject) => Promise<UpdateUserTestObject>;
declare const before02CollectResults: (testObject: UpdateUserTestObject) => Promise<UpdateUserTestObject>;
declare const after00TearDown: (testObject: UpdateUserTestObject) => Promise<UpdateUserTestObject>;
declare const test00: (testObject: UpdateUserTestObject) => UpdateUserTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
