"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const Types_1 = require("../../Types");
const updateUser = (request) => __awaiter(this, void 0, void 0, function* () {
    const client = new aws_sdk_1.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18',
        region: process.env.region || 'us-east-1',
    });
    const getUserRequest = {
        UserPoolId: request.customerId,
        Username: request.username,
    };
    const listGroupsForUser = {
        UserPoolId: request.customerId,
        Username: request.username,
    };
    const userData = yield Promise.all([
        client.adminGetUser(getUserRequest).promise(),
        client.adminListGroupsForUser(listGroupsForUser).promise(),
    ]);
    const updateUserAttributesRequest = {
        UserPoolId: request.customerId,
        Username: request.username,
        UserAttributes: request.userAttributes.map(att => ({ Name: att.name, Value: att.value })),
    };
    // checkGroups
    const addGroups = request.groups.filter(g => !userData[1].Groups.some(G => g.groupName === G.GroupName));
    const removeGroups = userData[1].Groups.filter(G => !request.groups.some(g => g.groupName === G.GroupName));
    // Add all to an array so that they an be wrapped in a Promise.all
    const combinedOperations = addGroups
        .map(g => client.adminAddUserToGroup({ UserPoolId: request.customerId, Username: request.username, GroupName: g.groupName }).promise())
        .concat(removeGroups.map(G => client.adminRemoveUserFromGroup({ UserPoolId: request.customerId, Username: request.username, GroupName: G.GroupName }).promise()));
    combinedOperations.push(client.adminUpdateUserAttributes(updateUserAttributesRequest).promise());
    if (userData[0].Enabled !== request.enabled) {
        combinedOperations.push(request.enabled ?
            client.adminEnableUser({ UserPoolId: request.customerId, Username: request.username }).promise() :
            client.adminDisableUser({ UserPoolId: request.customerId, Username: request.username }).promise());
    }
    const res = yield Promise.all(combinedOperations);
    return {
        username: request.username,
        enabled: request.enabled !== undefined ? request.enabled : userData[0].Enabled,
        userAttributes: request.userAttributes !== undefined ? request.userAttributes :
            userData[0].UserAttributes.map(Types_1.cognitoUserAttributeToAttribute),
        groups: request.groups !== undefined ? request.groups : userData[1].Groups.map(Types_1.cognitoGroupToGroup),
    };
});
exports.default = updateUser;
