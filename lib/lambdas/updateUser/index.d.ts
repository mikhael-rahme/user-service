import { UpdateUserRequest, UpdateUserResponse } from '../../Types';
declare const updateUser: (request: UpdateUserRequest) => Promise<UpdateUserResponse>;
export default updateUser;
