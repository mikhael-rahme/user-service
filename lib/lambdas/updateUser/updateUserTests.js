"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const createGroup_1 = require("../createGroup");
const AddUserToGroupTests = require("../addUserToGroup/addUserToGroupTests");
const getUser_1 = require("../getUser");
const _1 = require("./");
const createTestObject = () => ({
    environment: null,
    addUserToGroupTestObject: AddUserToGroupTests.createTestObject(),
    createGroupRequest: null,
    createGroupResponse: null,
    updateUserRequest: null,
    updateUserResponse: null,
    getUserRequest: null,
    getUserResponse: null,
    newEmail: `testEmail0@test.com`,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield AddUserToGroupTests.before00Setup(testObject.addUserToGroupTestObject);
            yield AddUserToGroupTests.before01Run(testObject.addUserToGroupTestObject);
            yield AddUserToGroupTests.before02CollectResults(testObject.addUserToGroupTestObject);
            const customerId = testObject.environment.createCustomerResponse.customerId;
            const username = testObject.addUserToGroupTestObject.createUserTestObject.username;
            testObject.createGroupRequest = { customerId, groupName: 'addGroup' };
            testObject.createGroupResponse = yield createGroup_1.default(testObject.createGroupRequest);
            testObject.updateUserRequest = {
                customerId,
                username,
                groups: [
                    { groupName: 'addGroup' },
                ],
                enabled: false,
                userAttributes: [
                    { name: 'email', value: testObject.newEmail },
                ],
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.updateUserResponse = yield _1.default(testObject.updateUserRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.updateUser(testObject.updateUserRequest);
        testObject.updateUserResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const customerId = testObject.environment.createCustomerResponse.customerId;
        const username = testObject.addUserToGroupTestObject.createUserTestObject.username;
        testObject.getUserRequest = {
            customerId,
            username,
        };
        testObject.getUserResponse = yield getUser_1.default(testObject.getUserRequest);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield AddUserToGroupTests.after00TearDown(testObject.addUserToGroupTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('updateUser', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('getUserResponse.userAttributes to include second email', () => {
            expect(testObject.getUserResponse.userAttributes).to.deep.include(testObject.updateUserRequest.userAttributes[0]);
        });
        it(`getUserResponse.groups to include addGroup and not ${testObject.addUserToGroupTestObject.createGroupTestObject.groupName}`, () => {
            expect(testObject.updateUserResponse.groups.length).to.equal(1);
            expect(testObject.updateUserResponse.groups[0]).to.deep.include({ groupName: testObject.createGroupRequest.groupName });
        });
        it('getUserResponse.enabled to be false', () => {
            expect(testObject.updateUserResponse.enabled).false;
        });
    });
    return testObject;
};
exports.test00 = test00;
