import { UserAttribute, Group, CreateGroupRequest, CreateGroupResponse, AddUserToGroupTestObject, GetUserRequest, GetUserResponse, UserServiceLambdaTestObject } from '../../Types';
export declare type UpdateUserRequest = {
    username: string;
    customerId: string;
    enabled?: boolean;
    groups?: Group[];
    userAttributes?: UserAttribute[];
};
export declare type UpdateUserResponse = {
    username: String;
    enabled?: boolean;
    groups?: Group[];
    userAttributes?: UserAttribute[];
};
export interface UpdateUserTestObject extends UserServiceLambdaTestObject {
    addUserToGroupTestObject: AddUserToGroupTestObject;
    createGroupRequest: CreateGroupRequest;
    createGroupResponse: CreateGroupResponse;
    updateUserRequest: UpdateUserRequest;
    updateUserResponse: UpdateUserResponse;
    getUserRequest: GetUserRequest;
    getUserResponse: GetUserResponse;
    newEmail: string;
    error: Error;
}
