"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const _1 = require("./");
const auth_1 = require("../../auth");
const CreateUserTests = require("../createUser/createUserTests");
const newPassword = 'someNewPassword123@#$';
const createTestObject = () => ({
    environment: null,
    createUserTestObject: CreateUserTests.createTestObject(),
    changeUserPasswordRequest: null,
    changeUserPasswordResponse: null,
    postChangeUserPasswordAuthorizeUserResponse: null,
    getUserRequest: null,
    getUserResponse: null,
    username: `testUsername-${Date.now()}`,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield CreateUserTests.before00Setup(testObject.createUserTestObject);
            yield CreateUserTests.before01Run(testObject.createUserTestObject);
            yield CreateUserTests.before02CollectResults(testObject.createUserTestObject);
        }
        catch (err) {
            console.log(err, JSON.stringify(testObject, null, 2));
            testObject.error = err;
        }
        const authorizeUserResponse = yield auth_1.authorizeUser({
            customerId: testObject.createUserTestObject.createUserRequest.customerId,
            clientId: testObject.createUserTestObject.createUserRequest.clientId,
            username: testObject.createUserTestObject.createUserRequest.username,
            password: testObject.createUserTestObject.createUserRequest.password,
        });
        testObject.changeUserPasswordRequest = {
            newPassword,
            accessToken: authorizeUserResponse.authenticationResponse.accessToken,
            oldPassword: testObject.createUserTestObject.password,
        };
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.changeUserPasswordResponse = yield _1.default(testObject.changeUserPasswordRequest);
        }
        catch (err) {
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.changeUserPassword(testObject.changeUserPasswordRequest);
        testObject.changeUserPasswordResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    testObject.postChangeUserPasswordAuthorizeUserResponse = yield auth_1.authorizeUser({
        customerId: testObject.createUserTestObject.createUserRequest.customerId,
        clientId: testObject.createUserTestObject.createUserRequest.clientId,
        username: testObject.createUserTestObject.createUserRequest.username,
        password: newPassword,
    });
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield CreateUserTests.after00TearDown(testObject.createUserTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('changetUserPassword', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('changeUserPasswordResponse return a success message', () => {
            expect(testObject.changeUserPasswordResponse).to.deep.equal({
                message: 'Success',
            });
        });
        it('should allow users to login with the new password', () => {
            expect(testObject.postChangeUserPasswordAuthorizeUserResponse.authenticationResponse).to.have.include.keys(['accessToken']);
        });
    });
    return testObject;
};
exports.test00 = test00;
