import { ChangeUserPasswordRequest, ChangeUserPasswordResponse } from './Types';
declare const changeUserPassword: (request: ChangeUserPasswordRequest) => Promise<ChangeUserPasswordResponse>;
export default changeUserPassword;
