import { ChangeUserPasswordTestObject } from './Types';
declare const createTestObject: () => ChangeUserPasswordTestObject;
declare const before00Setup: (testObject: ChangeUserPasswordTestObject) => Promise<ChangeUserPasswordTestObject>;
declare const before01Run: (testObject: ChangeUserPasswordTestObject) => Promise<ChangeUserPasswordTestObject>;
declare const run00Integration: (testObject: ChangeUserPasswordTestObject) => Promise<ChangeUserPasswordTestObject>;
declare const before02CollectResults: (testObject: ChangeUserPasswordTestObject) => Promise<ChangeUserPasswordTestObject>;
declare const after00TearDown: (testObject: ChangeUserPasswordTestObject) => Promise<ChangeUserPasswordTestObject>;
declare const test00: (testObject: ChangeUserPasswordTestObject) => ChangeUserPasswordTestObject;
export { before00Setup, run00Integration, before01Run, before02CollectResults, test00, after00TearDown, createTestObject };
