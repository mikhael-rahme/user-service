import { GetUserRequest, GetUserResponse, CreateUserTestObject, UserServiceLambdaTestObject } from '../../Types';
import { AuthorizeUserResponse } from '../../auth/authorizeUser/Types';
export declare type ChangeUserPasswordRequest = {
    accessToken: string;
    oldPassword: string;
    newPassword: string;
};
export declare type ChangeUserPasswordResponse = {
    error?: string;
    message?: string;
};
export interface ChangeUserPasswordTestObject extends UserServiceLambdaTestObject {
    createUserTestObject: CreateUserTestObject;
    changeUserPasswordRequest: ChangeUserPasswordRequest;
    changeUserPasswordResponse: ChangeUserPasswordResponse;
    postChangeUserPasswordAuthorizeUserResponse: AuthorizeUserResponse;
    getUserRequest: GetUserRequest;
    getUserResponse: GetUserResponse;
    username: string;
    error: Error;
}
