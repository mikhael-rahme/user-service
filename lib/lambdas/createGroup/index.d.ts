import { CreateGroupRequest } from './Types';
declare const createGroup: (request: CreateGroupRequest) => Promise<{
    groupName: string;
    description?: string;
}>;
export default createGroup;
