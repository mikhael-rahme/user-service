import { Group, ListGroupsRequest, ListGroupsResponse, UserServiceLambdaTestObject } from '../../Types';
export declare type CreateGroupRequest = {
    customerId: string;
    groupName: string;
    description?: string;
    precedence?: number;
    roleArn?: string;
};
export declare type CreateGroupResponse = Group;
export interface CreateGroupTestObject extends UserServiceLambdaTestObject {
    createGroupRequest: CreateGroupRequest;
    createGroupResponse: CreateGroupResponse;
    listGroupsRequest: ListGroupsRequest;
    listGroupsResponse: ListGroupsResponse;
    groupName: string;
    description: string;
    error: Error;
}
