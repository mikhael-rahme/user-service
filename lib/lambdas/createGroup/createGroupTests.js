"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const createGroup_1 = require("../createGroup");
const listGroups_1 = require("../listGroups");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createGroupRequest: null,
    createGroupResponse: null,
    listGroupsRequest: null,
    listGroupsResponse: null,
    groupName: `group-${Math.ceil(Math.random() * 999999999999)}`,
    description: 'testGroupDescription',
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.createGroupRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
                groupName: testObject.groupName,
                description: testObject.description,
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.createGroupResponse = yield createGroup_1.default(testObject.createGroupRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.createGroup(testObject.createGroupRequest);
        testObject.createGroupResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.listGroupsRequest = {
            customerId: testObject
                .environment
                .createCustomerResponse.customerId,
        };
        testObject.listGroupsResponse = yield listGroups_1.default(testObject.listGroupsRequest);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('createGroup', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('createGroupResponse to include username and groupName', () => {
            expect(testObject.createGroupResponse).to.deep.equal({
                groupName: testObject.groupName,
                description: testObject.description,
            });
        });
        it('listGroupsResponse to include testGroup', () => {
            const o0 = {
                groupName: testObject.groupName,
                description: testObject.description,
            };
            const o1s = testObject.listGroupsResponse.groups;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
    });
    return testObject;
};
exports.test00 = test00;
