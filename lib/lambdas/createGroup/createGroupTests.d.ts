import { CreateGroupTestObject } from '../../Types';
declare const createTestObject: () => CreateGroupTestObject;
declare const before00Setup: (testObject: CreateGroupTestObject) => Promise<CreateGroupTestObject>;
declare const before01Run: (testObject: CreateGroupTestObject) => Promise<CreateGroupTestObject>;
declare const run00Integration: (testObject: CreateGroupTestObject) => Promise<CreateGroupTestObject>;
declare const before02CollectResults: (testObject: CreateGroupTestObject) => Promise<CreateGroupTestObject>;
declare const after00TearDown: (testObject: CreateGroupTestObject) => Promise<CreateGroupTestObject>;
declare const test00: (testObject: CreateGroupTestObject) => CreateGroupTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
