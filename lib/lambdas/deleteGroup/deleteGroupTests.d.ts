import { DeleteGroupTestObject } from '../../Types';
declare const createTestObject: () => DeleteGroupTestObject;
declare const before00Setup: (testObject: DeleteGroupTestObject) => Promise<DeleteGroupTestObject>;
declare const before01Run: (testObject: DeleteGroupTestObject) => Promise<DeleteGroupTestObject>;
declare const run00Integration: (testObject: DeleteGroupTestObject) => Promise<DeleteGroupTestObject>;
declare const before02CollectResults: (testObject: DeleteGroupTestObject) => Promise<DeleteGroupTestObject>;
declare const after00TearDown: (testObject: DeleteGroupTestObject) => Promise<DeleteGroupTestObject>;
declare const test00: (testObject: DeleteGroupTestObject) => DeleteGroupTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
