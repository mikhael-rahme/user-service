import { DeleteGroupRequest, DeleteGroupResponse } from './Types';
declare const deleteGroup: (request: DeleteGroupRequest) => Promise<DeleteGroupResponse>;
export default deleteGroup;
