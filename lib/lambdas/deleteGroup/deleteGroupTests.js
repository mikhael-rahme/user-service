"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const _1 = require("./");
const listGroups_1 = require("../listGroups");
const CreateGroupTests = require("../createGroup/createGroupTests");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createGroupTestObject: CreateGroupTests.createTestObject(),
    deleteGroupRequest: null,
    deleteGroupResponse: null,
    listGroupsRequest: null,
    listGroupsResponse: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield CreateGroupTests.before00Setup(testObject.createGroupTestObject);
            yield CreateGroupTests.before01Run(testObject.createGroupTestObject);
            yield CreateGroupTests.before02CollectResults(testObject.createGroupTestObject);
            testObject.listGroupsRequest = testObject.createGroupTestObject.createGroupRequest;
            testObject.deleteGroupRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
                groupName: testObject
                    .createGroupTestObject.groupName,
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.deleteGroupResponse = yield _1.default(testObject.deleteGroupRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.deleteGroup(testObject.deleteGroupRequest);
        testObject.deleteGroupResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.listGroupsResponse = yield listGroups_1.default(testObject.listGroupsRequest);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield CreateGroupTests.after00TearDown(testObject.createGroupTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('deleteGroup', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('deleteGroupResponse to deep include deleteGroupRequest', () => {
            expect(testObject.deleteGroupResponse).to.deep.equal({
                groupName: testObject.deleteGroupRequest.groupName,
            });
        });
        it('listUsersResponse to not include groupName', () => {
            const o0 = {
                groupName: testObject.deleteGroupRequest.groupName,
            };
            const o1s = testObject.listGroupsResponse.groups;
            expect(o1s.some(o1 => subsetOf(o0, o1))).false;
        });
    });
    return testObject;
};
exports.test00 = test00;
