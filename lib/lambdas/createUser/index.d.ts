import { CreateUserRequest } from '../../Types';
declare const createUser: (request: CreateUserRequest) => Promise<{
    username: string;
    userAttributes: {
        name: string;
        value: string;
    }[];
    status: string;
    enabled: boolean;
    groups: {
        groupName: string;
        description?: string;
    }[];
}>;
export default createUser;
