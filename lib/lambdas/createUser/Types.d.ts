import { User, Group, UserAttribute, GetUserRequest, GetUserResponse, CreateGroupRequest, CreateGroupResponse, UserServiceLambdaTestObject } from '../../Types';
export declare type CreateUserRequest = {
    customerId: string;
    clientId: string;
    username: string;
    password: string;
    groups?: Group[];
    userAttributes?: UserAttribute[];
};
export declare type CreateUserResponse = User;
export interface CreateUserTestObject extends UserServiceLambdaTestObject {
    createGroupRequest: CreateGroupRequest;
    createGroupResponse: CreateGroupResponse;
    createUserRequest: CreateUserRequest;
    createUserResponse: CreateUserResponse;
    getUserRequest: GetUserRequest;
    getUserResponse: GetUserResponse;
    username: string;
    password: string;
    email: string;
    error: Error;
}
