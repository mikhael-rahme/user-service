import { CreateUserTestObject } from '../../Types';
declare const createTestObject: () => CreateUserTestObject;
declare const before00Setup: (testObject: CreateUserTestObject) => Promise<CreateUserTestObject>;
declare const before01Run: (testObject: CreateUserTestObject) => Promise<CreateUserTestObject>;
declare const run00Integration: (testObject: CreateUserTestObject) => Promise<CreateUserTestObject>;
declare const before02CollectResults: (testObject: CreateUserTestObject) => Promise<CreateUserTestObject>;
declare const after00TearDown: (testObject: CreateUserTestObject) => Promise<CreateUserTestObject>;
declare const test00: (testObject: CreateUserTestObject) => CreateUserTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
