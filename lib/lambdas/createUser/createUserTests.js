"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const _1 = require("./");
const getUser_1 = require("../getUser");
const createGroup_1 = require("../createGroup");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createUserRequest: null,
    createUserResponse: null,
    createGroupRequest: null,
    createGroupResponse: null,
    getUserRequest: null,
    getUserResponse: null,
    username: `user-${Math.ceil(Math.random() * 999999999999)}`,
    password: '1testPassword4you!',
    email: 'test@test.com',
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { customerId, clientId } = testObject.environment.createCustomerResponse;
            testObject.createGroupRequest = {
                customerId,
                groupName: `group-${Math.ceil(Math.random() * 999999999999)}`,
            };
            testObject.createGroupResponse = yield createGroup_1.default(testObject.createGroupRequest);
            testObject.getUserRequest = {
                customerId,
                username: testObject.username,
            };
            testObject.createUserRequest = {
                customerId,
                clientId,
                username: testObject.username,
                password: testObject.password,
                userAttributes: [
                    { name: 'email', value: testObject.email },
                ],
                groups: [
                    { groupName: testObject.createGroupRequest.groupName },
                ],
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.createUserResponse = yield _1.default(testObject.createUserRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.createUser(testObject.createUserRequest);
        testObject.createUserResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.getUserResponse = yield getUser_1.default(testObject.getUserRequest);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('createUser', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('createUserResponse to include username', () => {
            expect(testObject.createUserResponse).to.deep.include({
                username: testObject.username,
                status: 'CONFIRMED',
                enabled: true,
            });
        });
        it('createUserResponse.email to equal testObject.email', () => {
            expect(testObject.createUserResponse.userAttributes.find(x => x.name === 'email').value).to.equal(testObject.email);
        });
        it('getUserResponse to include testUser', () => {
            const o0 = {
                username: testObject.username,
                userAttributes: [
                    { name: 'email', value: testObject.email },
                ],
                groups: [
                    { groupName: testObject.createGroupRequest.groupName },
                ],
            };
            const o1 = testObject.getUserResponse;
            expect(subsetOf(o0, o1)).true;
        });
    });
    return testObject;
};
exports.test00 = test00;
