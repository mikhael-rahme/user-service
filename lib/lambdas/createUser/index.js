"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const Types_1 = require("../../Types");
const authorizeUser_1 = require("../../auth/authorizeUser");
const respondToNewPasswordRequired_1 = require("../../auth/respondToNewPasswordRequired");
const createUser = (request) => __awaiter(this, void 0, void 0, function* () {
    const client = new aws_sdk_1.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18',
        region: process.env.region || 'us-east-1',
    });
    const createUserRequest = {
        UserPoolId: request.customerId,
        Username: request.username,
        UserAttributes: request.userAttributes ?
            request.userAttributes.map(att => ({ Name: att.name, Value: att.value })) : undefined,
        TemporaryPassword: `${request.password}1`,
    };
    if (!createUserRequest.UserAttributes || createUserRequest.UserAttributes.length === 0) {
        createUserRequest.UserAttributes = [{ Name: 'email', Value: 'noemail@iterrex.com' }];
    }
    else if (!createUserRequest.UserAttributes.find(x => x.Name === 'email')) {
        createUserRequest.UserAttributes.push({ Name: 'email', Value: 'noemail@iterrex.com' });
    }
    const cognitoCreateUserResponse = yield client.adminCreateUser(createUserRequest).promise();
    const authorizeUserResponse = yield authorizeUser_1.default({
        customerId: request.customerId,
        clientId: request.clientId,
        username: request.username,
        password: `${request.password}1`,
    });
    const respondToNewPasswordRequiredResponse = yield respondToNewPasswordRequired_1.default({
        customerId: request.customerId,
        clientId: request.clientId,
        username: request.username,
        session: authorizeUserResponse.session,
        newPassword: request.password,
    });
    if (request.groups) {
        yield Promise.all(request.groups.map(group => client.adminAddUserToGroup({
            UserPoolId: request.customerId,
            Username: request.username,
            GroupName: group.groupName,
        }).promise()));
    }
    // Manually setting to confirmed, because that'll be the status, if nothing errors out.
    cognitoCreateUserResponse.User.UserStatus = 'CONFIRMED';
    const Groups = request.groups ? request.groups.map(g => ({ GroupName: g.groupName })) : [];
    return Types_1.cognitoUserToUser(Object.assign({ Groups }, cognitoCreateUserResponse.User));
});
exports.default = createUser;
