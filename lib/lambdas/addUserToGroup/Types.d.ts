import { CreateGroupTestObject, CreateUserTestObject, ListUsersInGroupRequest, ListUsersInGroupResponse, UserServiceLambdaTestObject } from '../../Types';
export declare type AddUserToGroupRequest = {
    customerId: string;
    username: string;
    groupName: string;
};
export declare type AddUserToGroupResponse = {
    username: string;
    groupName: string;
};
export interface AddUserToGroupTestObject extends UserServiceLambdaTestObject {
    createGroupTestObject: CreateGroupTestObject;
    createUserTestObject: CreateUserTestObject;
    addUserToGroupRequest: AddUserToGroupRequest;
    addUserToGroupResponse: AddUserToGroupResponse;
    listUsersInGroupRequest: ListUsersInGroupRequest;
    listUsersInGroupResponse: ListUsersInGroupResponse;
    error: Error;
}
