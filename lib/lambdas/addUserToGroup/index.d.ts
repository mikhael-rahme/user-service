import { AddUserToGroupRequest, AddUserToGroupResponse } from './Types';
declare const addUserToGroup: (request: AddUserToGroupRequest) => Promise<AddUserToGroupResponse>;
export default addUserToGroup;
