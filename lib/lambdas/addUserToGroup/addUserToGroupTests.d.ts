import { AddUserToGroupTestObject } from '../../Types';
declare const createTestObject: () => AddUserToGroupTestObject;
declare const before00Setup: (testObject: AddUserToGroupTestObject) => Promise<AddUserToGroupTestObject>;
declare const before01Run: (testObject: AddUserToGroupTestObject) => Promise<AddUserToGroupTestObject>;
declare const run00Integration: (testObject: AddUserToGroupTestObject) => Promise<AddUserToGroupTestObject>;
declare const before02CollectResults: (testObject: AddUserToGroupTestObject) => Promise<AddUserToGroupTestObject>;
declare const after00TearDown: (testObject: AddUserToGroupTestObject) => Promise<AddUserToGroupTestObject>;
declare const test00: (testObject: AddUserToGroupTestObject) => AddUserToGroupTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
