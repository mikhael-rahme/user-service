"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const listUsersInGroup_1 = require("../listUsersInGroup");
const _1 = require("./");
const createUser_1 = require("../createUser");
const CreateGroupTests = require("../createGroup/createGroupTests");
const CreateUserTests = require("../createUser/createUserTests");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createGroupTestObject: CreateGroupTests.createTestObject(),
    createUserTestObject: CreateUserTests.createTestObject(),
    addUserToGroupRequest: null,
    addUserToGroupResponse: null,
    listUsersInGroupRequest: null,
    listUsersInGroupResponse: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield CreateGroupTests.before00Setup(testObject.createGroupTestObject);
            yield CreateGroupTests.before01Run(testObject.createGroupTestObject);
            yield CreateGroupTests.before02CollectResults(testObject.createGroupTestObject);
            const { groupName } = testObject.createGroupTestObject;
            const { username, password } = testObject.createUserTestObject;
            const { clientId, customerId } = testObject.environment.createCustomerResponse;
            testObject.createUserTestObject.createUserRequest = {
                username,
                password,
                customerId,
                clientId,
            };
            testObject.createUserTestObject.createUserResponse = yield createUser_1.default(testObject.createUserTestObject.createUserRequest);
            testObject.addUserToGroupRequest = {
                customerId,
                username,
                groupName,
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.addUserToGroupResponse = yield _1.default(testObject.addUserToGroupRequest);
        }
        catch (err) {
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.addUserToGroup(testObject.addUserToGroupRequest);
        testObject.addUserToGroupResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.listUsersInGroupRequest = {
            customerId: testObject
                .environment
                .createCustomerResponse
                .customerId,
            groupName: testObject
                .createGroupTestObject
                .groupName,
        };
        testObject.listUsersInGroupResponse = yield listUsersInGroup_1.default(testObject.listUsersInGroupRequest);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield CreateGroupTests.after00TearDown(testObject.createGroupTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('addUserToGroup', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('addUserToGroupResponse to include username and groupName', () => {
            expect(testObject.addUserToGroupResponse).to.deep.equal({
                username: testObject
                    .createUserTestObject
                    .username,
                groupName: testObject
                    .createGroupTestObject
                    .groupName,
            });
        });
        it('listUsersInGroup to include testUser', () => {
            const o0 = {
                username: testObject
                    .createUserTestObject
                    .username,
                enabled: true,
            };
            const o1s = testObject.listUsersInGroupResponse.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
    });
    return testObject;
};
exports.test00 = test00;
