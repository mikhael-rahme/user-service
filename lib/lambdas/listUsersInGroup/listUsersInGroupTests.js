"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const _1 = require("./");
const AddUserToGroupTests = require("../addUserToGroup/addUserToGroupTests");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    addUserToGroupTestObject: AddUserToGroupTests.createTestObject(),
    listUsersInGroupRequest: null,
    listUsersInGroupResponse: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield AddUserToGroupTests.before00Setup(testObject.addUserToGroupTestObject);
            yield AddUserToGroupTests.before01Run(testObject.addUserToGroupTestObject);
            yield AddUserToGroupTests.before02CollectResults(testObject.addUserToGroupTestObject);
            testObject.listUsersInGroupRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
                groupName: testObject
                    .addUserToGroupTestObject
                    .createGroupTestObject
                    .groupName,
            };
        }
        catch (err) {
            console.log(err, JSON.stringify(testObject, null, 2));
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.listUsersInGroupResponse = yield _1.default(testObject.listUsersInGroupRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.listUsersInGroup(testObject.listUsersInGroupRequest);
        testObject.listUsersInGroupResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    // nada
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield AddUserToGroupTests.after00TearDown(testObject.addUserToGroupTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('listUsersInGroup', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('listUsersInGroupResponse to include createUserResponse object', () => {
            const o0 = {
                username: testObject
                    .addUserToGroupTestObject
                    .createUserTestObject
                    .createUserResponse.username,
                status: testObject
                    .addUserToGroupTestObject
                    .createUserTestObject
                    .createUserResponse.status,
                enabled: testObject
                    .addUserToGroupTestObject
                    .createUserTestObject
                    .createUserResponse.enabled,
            };
            const o1s = testObject.listUsersInGroupResponse.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
    });
    return testObject;
};
exports.test00 = test00;
