import { ListUsersInGroupRequest, ListUsersInGroupResponse } from './Types';
declare const listUsersInGroup: (request: ListUsersInGroupRequest) => Promise<ListUsersInGroupResponse>;
export default listUsersInGroup;
