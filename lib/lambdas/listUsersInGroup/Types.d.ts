import { User, AddUserToGroupTestObject, UserServiceLambdaTestObject } from '../../Types';
export declare type ListUsersInGroupRequest = {
    customerId: string;
    groupName: string;
    limit?: number;
    paginationToken?: string;
};
export declare type ListUsersInGroupResponse = {
    users: User[];
    paginationToken?: string;
};
export interface ListUsersInGroupTestObject extends UserServiceLambdaTestObject {
    addUserToGroupTestObject: AddUserToGroupTestObject;
    listUsersInGroupRequest: ListUsersInGroupRequest;
    listUsersInGroupResponse: ListUsersInGroupResponse;
    error: Error;
}
