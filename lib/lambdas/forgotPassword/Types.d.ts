import { GetUserRequest, GetUserResponse, CreateUserTestObject, UserServiceLambdaTestObject } from '../../Types';
export declare type ForgotPasswordRequest = {
    clientId: string;
    username: string;
};
export declare type ForgotPasswordResponse = {
    username: string;
};
export interface ForgotPasswordTestObject extends UserServiceLambdaTestObject {
    createUserTestObject: CreateUserTestObject;
    forgotPasswordRequest: ForgotPasswordRequest;
    forgotPasswordResponse: ForgotPasswordResponse;
    getUserRequest: GetUserRequest;
    getUserResponse: GetUserResponse;
    error: Error;
}
