import { ListGroupsRequest, ListGroupsResponse } from './Types';
declare const listGroups: (request: ListGroupsRequest) => Promise<ListGroupsResponse>;
export default listGroups;
