"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const toolkit_1 = require("toolkit");
const utils_1 = require("../../utils");
const _1 = require("./");
const CreateGroupTests = require("../createGroup/createGroupTests");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createGroupTestObject: CreateGroupTests.createTestObject(),
    listGroupsRequest: null,
    listGroupsResponse: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield CreateGroupTests.before00Setup(testObject.createGroupTestObject);
            yield CreateGroupTests.before01Run(testObject.createGroupTestObject);
            yield CreateGroupTests.before02CollectResults(testObject.createGroupTestObject);
            testObject.listGroupsRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.listGroupsResponse = yield _1.default(testObject.listGroupsRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.listGroups(testObject.listGroupsRequest);
        testObject.listGroupsResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    // nada
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield CreateGroupTests.after00TearDown(testObject.createGroupTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('listGroups', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('listGroupsResponse to include createGroupResponse object', () => {
            const o0 = { groupName: testObject.createGroupTestObject.groupName };
            const o1s = testObject.listGroupsResponse.groups;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
    });
    return testObject;
};
exports.test00 = test00;
