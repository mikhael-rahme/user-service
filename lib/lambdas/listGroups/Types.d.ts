import { Group, CreateGroupTestObject, UserServiceLambdaTestObject } from '../../Types';
export declare type ListGroupsRequest = {
    customerId: string;
    limit?: number;
    paginationToken?: string;
};
export declare type ListGroupsResponse = {
    groups: Group[];
    paginationToken?: string;
};
export interface ListGroupsTestObject extends UserServiceLambdaTestObject {
    createGroupTestObject: CreateGroupTestObject;
    listGroupsRequest: ListGroupsRequest;
    listGroupsResponse: ListGroupsResponse;
    error: Error;
}
