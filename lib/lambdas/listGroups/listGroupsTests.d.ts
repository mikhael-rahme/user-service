import { ListGroupsTestObject } from '../../Types';
declare const createTestObject: () => ListGroupsTestObject;
declare const before00Setup: (testObject: ListGroupsTestObject) => Promise<ListGroupsTestObject>;
declare const before01Run: (testObject: ListGroupsTestObject) => Promise<ListGroupsTestObject>;
declare const run00Integration: (testObject: ListGroupsTestObject) => Promise<ListGroupsTestObject>;
declare const before02CollectResults: (testObject: ListGroupsTestObject) => Promise<ListGroupsTestObject>;
declare const after00TearDown: (testObject: ListGroupsTestObject) => Promise<ListGroupsTestObject>;
declare const test00: (testObject: ListGroupsTestObject) => ListGroupsTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
