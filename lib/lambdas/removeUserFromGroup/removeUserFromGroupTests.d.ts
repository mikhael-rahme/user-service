import { RemoveUserFromGroupTestObject } from '../../Types';
declare const createTestObject: () => RemoveUserFromGroupTestObject;
declare const before00Setup: (testObject: RemoveUserFromGroupTestObject) => Promise<RemoveUserFromGroupTestObject>;
declare const before01Run: (testObject: RemoveUserFromGroupTestObject) => Promise<RemoveUserFromGroupTestObject>;
declare const run00Integration: (testObject: RemoveUserFromGroupTestObject) => Promise<RemoveUserFromGroupTestObject>;
declare const before02CollectResults: (testObject: RemoveUserFromGroupTestObject) => Promise<RemoveUserFromGroupTestObject>;
declare const after00TearDown: (testObject: RemoveUserFromGroupTestObject) => Promise<RemoveUserFromGroupTestObject>;
declare const test00: (testObject: RemoveUserFromGroupTestObject) => RemoveUserFromGroupTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
