"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const _1 = require("./");
const listUsersInGroup_1 = require("../listUsersInGroup");
const AddUserToGroupTests = require("../addUserToGroup/addUserToGroupTests");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    addUserToGroupTestObject: AddUserToGroupTests.createTestObject(),
    removeUserFromGroupRequest: null,
    removeUserFromGroupResponse: null,
    listUsersInGroupRequest: null,
    listUsersInGroupResponse: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield AddUserToGroupTests.before00Setup(testObject.addUserToGroupTestObject);
            yield AddUserToGroupTests.before01Run(testObject.addUserToGroupTestObject);
            yield AddUserToGroupTests.before02CollectResults(testObject.addUserToGroupTestObject);
            testObject.removeUserFromGroupRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
                groupName: testObject
                    .addUserToGroupTestObject
                    .createGroupTestObject
                    .groupName,
                username: testObject
                    .addUserToGroupTestObject
                    .createUserTestObject
                    .username,
            };
            testObject.listUsersInGroupRequest = testObject.addUserToGroupTestObject.listUsersInGroupRequest;
        }
        catch (err) {
            console.log(err, JSON.stringify(testObject, null, 2));
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.removeUserFromGroupResponse = yield _1.default(testObject.removeUserFromGroupRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.removeUserFromGroup(testObject.removeUserFromGroupRequest);
        testObject.removeUserFromGroupResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.listUsersInGroupResponse = yield listUsersInGroup_1.default(testObject.listUsersInGroupRequest);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield AddUserToGroupTests.after00TearDown(testObject.addUserToGroupTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('removeUserFromGroup', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('listUsersInGroupResponse0 to include createUserResponse object', () => {
            const { username, status, enabled } = testObject
                .addUserToGroupTestObject
                .createUserTestObject
                .createUserResponse;
            const o0 = { username, status, enabled };
            const o1s = testObject.addUserToGroupTestObject.listUsersInGroupResponse.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
        it('removeUserFromGroupResponse to equal removeUserFromGroupRequest', () => {
            expect(testObject.removeUserFromGroupResponse).to.deep.equal({
                username: testObject.removeUserFromGroupRequest.username,
                groupName: testObject.removeUserFromGroupRequest.groupName,
            });
        });
        it('listUsersInGroupResponse1 to not return users', () => {
            const { username, status, enabled } = testObject
                .addUserToGroupTestObject
                .createUserTestObject
                .createUserResponse;
            const o0 = { username, status, enabled };
            const o1s = testObject.listUsersInGroupResponse.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).false;
        });
    });
    return testObject;
};
exports.test00 = test00;
