import { RemoveUserFromGroupRequest, RemoveUserFromGroupResponse } from './Types';
declare const removeUserFromGroup: (request: RemoveUserFromGroupRequest) => Promise<RemoveUserFromGroupResponse>;
export default removeUserFromGroup;
