"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const aws_sdk_1 = require("aws-sdk");
const deleteGroup = (request) => __awaiter(this, void 0, void 0, function* () {
    const client = new aws_sdk_1.CognitoIdentityServiceProvider({
        apiVersion: '2016-04-18',
        region: process.env.region || 'us-east-1',
    });
    const deleteUserRequest = {
        UserPoolId: request.customerId,
        Username: request.username,
    };
    yield client.adminDeleteUser(deleteUserRequest).promise();
    return {
        username: request.username,
    };
});
exports.default = deleteGroup;
