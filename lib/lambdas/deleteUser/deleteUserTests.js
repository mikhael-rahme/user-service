"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const { describe, it } = require('mocha');
const expect = require('chai').expect;
const utils_1 = require("../../utils");
const _1 = require("./");
const listUsers_1 = require("../listUsers");
const CreateUserTests = require("../createUser/createUserTests");
const toolkit_1 = require("toolkit");
const { subsetOf } = toolkit_1.testUtils;
const createTestObject = () => ({
    environment: null,
    createUserTestObject: CreateUserTests.createTestObject(),
    listUsersRequest0: null,
    listUsersResponse0: null,
    deleteUserRequest: null,
    deleteUserResponse: null,
    listUsersRequest1: null,
    listUsersResponse1: null,
    error: null,
});
exports.createTestObject = createTestObject;
const before00Setup = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield CreateUserTests.before00Setup(testObject.createUserTestObject);
            yield CreateUserTests.before01Run(testObject.createUserTestObject);
            yield CreateUserTests.before02CollectResults(testObject.createUserTestObject);
            testObject.listUsersRequest0 = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
            };
            testObject.listUsersResponse0 = yield listUsers_1.default(testObject.listUsersRequest0);
            testObject.deleteUserRequest = {
                customerId: testObject
                    .environment
                    .createCustomerResponse.customerId,
                username: testObject
                    .createUserTestObject
                    .username,
            };
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before00Setup = before00Setup;
const before01Run = function (testObject) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            testObject.deleteUserResponse = yield _1.default(testObject.deleteUserRequest);
        }
        catch (err) {
            console.log('err.message:', err.message);
            testObject.error = err;
        }
        return testObject;
    });
};
exports.before01Run = before01Run;
const run00Integration = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        const response = yield utils_1.client.deleteUser(testObject.deleteUserRequest);
        testObject.deleteUserResponse = response.data;
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.run00Integration = run00Integration;
const before02CollectResults = (testObject) => __awaiter(this, void 0, void 0, function* () {
    try {
        testObject.listUsersRequest1 = testObject.listUsersRequest0;
        testObject.listUsersResponse1 = yield listUsers_1.default(testObject.listUsersRequest1);
    }
    catch (err) {
        console.log('err.message:', err.message);
        testObject.error = err;
    }
    return testObject;
});
exports.before02CollectResults = before02CollectResults;
const after00TearDown = (testObject) => __awaiter(this, void 0, void 0, function* () {
    yield CreateUserTests.after00TearDown(testObject.createUserTestObject);
    return testObject;
});
exports.after00TearDown = after00TearDown;
const test00 = (testObject) => {
    describe('deleteUser', () => {
        it('should not have errored', () => {
            expect(testObject.error).to.be.null;
        });
        it('deleteUserResponse to deep equal deleteUserRequest', () => {
            expect(testObject.deleteUserResponse).to.deep.equal({
                username: testObject.deleteUserRequest.username,
            });
        });
        it('listUsersResponse0 to include username', () => {
            const o0 = {
                username: testObject
                    .createUserTestObject
                    .username,
            };
            const o1s = testObject.listUsersResponse0.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).true;
        });
        it('listUsersResponse1 to not include username', () => {
            const o0 = {
                username: testObject
                    .createUserTestObject
                    .username,
            };
            const o1s = testObject.listUsersResponse1.users;
            expect(o1s.some(o1 => subsetOf(o0, o1))).false;
        });
    });
    return testObject;
};
exports.test00 = test00;
