import { DeleteUserRequest, DeleteUserResponse } from './Types';
declare const deleteGroup: (request: DeleteUserRequest) => Promise<DeleteUserResponse>;
export default deleteGroup;
