import { ListUsersRequest, ListUsersResponse, DeleteUserRequest, DeleteUserResponse, CreateUserTestObject, UserServiceLambdaTestObject } from '../../Types';
export interface DeleteUserTestObject extends UserServiceLambdaTestObject {
    createUserTestObject: CreateUserTestObject;
    listUsersRequest0: ListUsersRequest;
    listUsersResponse0: ListUsersResponse;
    deleteUserRequest: DeleteUserRequest;
    deleteUserResponse: DeleteUserResponse;
    listUsersRequest1: ListUsersRequest;
    listUsersResponse1: ListUsersResponse;
    error: Error;
}
export declare type DeleteUserRequest = {
    customerId: string;
    username: string;
};
export declare type DeleteUserResponse = {
    username: string;
};
