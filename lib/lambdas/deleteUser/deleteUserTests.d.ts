import { DeleteUserTestObject } from '../../Types';
declare const createTestObject: () => DeleteUserTestObject;
declare const before00Setup: (testObject: DeleteUserTestObject) => Promise<DeleteUserTestObject>;
declare const before01Run: (testObject: DeleteUserTestObject) => Promise<DeleteUserTestObject>;
declare const run00Integration: (testObject: DeleteUserTestObject) => Promise<DeleteUserTestObject>;
declare const before02CollectResults: (testObject: DeleteUserTestObject) => Promise<DeleteUserTestObject>;
declare const after00TearDown: (testObject: DeleteUserTestObject) => Promise<DeleteUserTestObject>;
declare const test00: (testObject: DeleteUserTestObject) => DeleteUserTestObject;
export { before00Setup, before01Run, run00Integration, before02CollectResults, test00, after00TearDown, createTestObject };
