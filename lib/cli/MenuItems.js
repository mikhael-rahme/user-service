"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const inquirer_1 = require("inquirer");
const Table = require("cli-table");
const addUserToGroup_1 = require("../lambdas/addUserToGroup");
const createGroup_1 = require("../lambdas/createGroup");
const createUser_1 = require("../lambdas/createUser");
const deleteGroup_1 = require("../lambdas/deleteGroup");
const deleteUser_1 = require("../lambdas/deleteUser");
const getUser_1 = require("../lambdas/getUser");
const listGroups_1 = require("../lambdas/listGroups");
const listUsers_1 = require("../lambdas/listUsers");
const listUsersInGroup_1 = require("../lambdas/listUsersInGroup");
const removeUserFromGroup_1 = require("../lambdas/removeUserFromGroup");
const resetUserPassword_1 = require("../lambdas/resetUserPassword");
const updateUser_1 = require("../lambdas/updateUser");
const customer_service_1 = require("customer-service");
const getMenuItems = (caller, stage) => __awaiter(this, void 0, void 0, function* () {
    const client = customer_service_1.default(caller, stage);
    try {
        const result = yield client.listCustomers();
        const listCustomersResponse = yield inquirer_1.prompt([
            {
                type: 'list',
                name: 'selectedCustomer',
                message: 'Select a Customer',
                choices: result.data.map(customer => customer.customerName),
            },
        ]);
        const selectedCustomerName = listCustomersResponse['selectedCustomer'];
        var selectedCustomer = result.data.find(customer => selectedCustomerName === customer.customerName);
    }
    catch (err) {
        throw new Error(err);
    }
    return {
        listUsers: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const request = {
                    customerId: selectedCustomer.customerId,
                };
                const userList = yield listUsers_1.default(request);
                const head = ['USERS'];
                const table = new Table({ head });
                table.push(...userList.users.map(users => [
                    users.username,
                ]));
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        listGroups: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const request = {
                    customerId: selectedCustomer.customerId,
                };
                const groupList = yield listGroups_1.default(request);
                const head = ['GROUPS'];
                const table = new Table({ head });
                table.push(...groupList.groups.map(groups => [
                    groups.groupName,
                ]));
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        listUsersinGroup: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const listUsersInGroupResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'groupName',
                        message: 'Input Group Name',
                        validate: input => !!input || 'You must give a value',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    groupName: listUsersInGroupResponse['groupName'],
                };
                const usersInGroupList = yield listUsersInGroup_1.default(request);
                const head = ['GROUP', 'USER'];
                const table = new Table({ head });
                table.push(...usersInGroupList.users.map(users => [
                    request.groupName,
                    users.username,
                ]));
                console.log(table.toString());
                /*
                if(user.hasOwnProperty("username")){
                  console.log("HAS username");
                }
                */
            }
            catch (err) {
                console.log(err);
            }
        }),
        getUser: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const getUserResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input username',
                        validate: input => !!input || 'You must give a value!',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    username: getUserResponse['username'],
                };
                const user = yield getUser_1.default(request);
                var head = ['USERNAME', 'STATUS', 'ENABLED'];
                const table = new Table({ head });
                table.push([
                    user.username,
                    user.status,
                    user.enabled,
                ]);
                head = ['ATTRIBUTE', 'VALUE'];
                const table2 = new Table({ head });
                table2.push(...user.userAttributes.map(userAttributes => [
                    userAttributes.name,
                    userAttributes.value,
                ]));
                console.log(table.toString());
                console.log(table2.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        addUserToGroup: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const addUserToGroupResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input username',
                        validate: input => !!input || 'You must give a value',
                    },
                    {
                        type: 'input',
                        name: 'groupName',
                        message: 'Input groupName',
                        validate: input => !!input || 'You must give a value',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    username: addUserToGroupResponse['username'],
                    groupName: addUserToGroupResponse['groupName'],
                };
                const addUser = yield addUserToGroup_1.default(request);
                const head = ['USER ADDED', 'TO GROUP'];
                const table = new Table({ head });
                table.push([
                    addUser.username,
                    addUser.groupName,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        createUser: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const createUserResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'clientID',
                        message: 'Input clientID',
                        validate: input => !!input || 'You must give a value',
                    },
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input username',
                        validate: input => !!input || 'You must give a value',
                    },
                    {
                        type: 'input',
                        name: 'password',
                        message: 'Input password',
                        validate: input => !!input || 'You must give a value',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    clientId: createUserResponse['clientID'],
                    username: createUserResponse['username'],
                    password: createUserResponse['password'],
                };
                const createNewUser = yield createUser_1.default(request);
                const head = ['USER CREATED'];
                const table = new Table({ head });
                table.push([
                    createNewUser.username,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        createGroup: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const createGroupResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'groupName',
                        message: 'Input groupName',
                        validate: input => !!input || 'You must give a value',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    groupName: createGroupResponse['groupName'],
                };
                const newGroup = yield createGroup_1.default(request);
                const head = ['GROUP CREATED'];
                const table = new Table({ head });
                table.push([
                    request.groupName,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        deleteUser: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const deleteUserResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input Username',
                        validate: input => !!input || 'You must give a value!',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    username: deleteUserResponse['username'],
                };
                const delUser = yield deleteUser_1.default(request);
                const head = ['USER DELETED'];
                const table = new Table({ head });
                table.push([
                    delUser.username,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        deleteGroup: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const deleteGroupResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'groupName',
                        message: 'Input Group Name',
                        validate: input => !!input || 'You must give a value!',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    groupName: deleteGroupResponse['groupName'],
                };
                const delGroup = yield deleteGroup_1.default(request);
                const head = ['GROUP DELETED'];
                const table = new Table({ head });
                table.push([
                    delGroup.groupName,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        //broken?
        removeUserFromGroup: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const removeUserFromGroupResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input Username',
                        validate: input => !!input || 'You must give a value!',
                    },
                    {
                        type: 'input',
                        name: 'groupName',
                        message: 'Input Group Name',
                        validate: input => !!input || 'You must give a value!',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    username: removeUserFromGroup_1.default['username'],
                    groupName: removeUserFromGroup_1.default['groupName'],
                };
                const delUserFromGroup = yield removeUserFromGroup_1.default(request);
                const head = ['USER REMOVED', 'FROM GROUP'];
                const table = new Table({ head });
                table.push([
                    delUserFromGroup.username,
                    delUserFromGroup.groupName,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        resetUserPassword: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const resetUserPasswordResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input Username',
                        validate: input => !!input || 'You must give a value!',
                    },
                ]);
                const request = {
                    customerId: selectedCustomer.customerId,
                    username: resetUserPasswordResponse['username'],
                };
                const resetUser = yield resetUserPassword_1.default(request);
                const head = ['PASSWORD RESET ON USER'];
                const table = new Table({ head });
                table.push([
                    resetUser.username,
                ]);
                console.log(table.toString());
            }
            catch (err) {
                console.log(err);
            }
        }),
        updateUser: () => __awaiter(this, void 0, void 0, function* () {
            try {
                const getUsernameResponse = yield inquirer_1.prompt([
                    {
                        type: 'input',
                        name: 'username',
                        message: 'Input Username',
                        validate: input => !!input || 'You must give a value!',
                    },
                ]);
                const askUpdateEnabledResponse = yield inquirer_1.prompt([
                    {
                        type: 'confirm',
                        name: 'askEnabled',
                        message: 'Updated Enabled Property?',
                    }
                ]);
                if (askUpdateEnabledResponse['askEnabled'] == true) {
                    var updateEnabledResponse = yield inquirer_1.prompt([
                        {
                            type: 'confirm',
                            name: 'updatedEnabled',
                            message: 'Enabled?',
                        },
                    ]);
                }
                const askUpdateGroupsResponse = yield inquirer_1.prompt([
                    {
                        type: 'confirm',
                        name: 'askGroups',
                        message: 'Update Groups?',
                    }
                ]);
                if (askUpdateGroupsResponse['askGroups'] == true) {
                    var updateGroupsResponse = yield inquirer_1.prompt([
                        {
                            type: 'input',
                            name: 'updatedGroups',
                            message: 'Input Group',
                            validate: input => !!input || 'You must give a value!',
                        },
                    ]);
                }
                const askUpdateAttResponse = yield inquirer_1.prompt([
                    {
                        type: 'confirm',
                        name: 'askGroups',
                        message: 'Update Attributes?',
                    }
                ]);
                if (askUpdateAttResponse['askGroups'] == true) {
                    var updateAttResponse = yield inquirer_1.prompt([
                        {
                            type: 'input',
                            name: 'updatedAtt',
                            message: 'Input Attribute',
                            validate: input => !!input || 'You must give a value!',
                        },
                        {
                            type: 'input',
                            name: 'updatedValue',
                            message: 'Input Attribute Value',
                            validate: input => !!input || 'You must give a value!',
                        },
                    ]);
                }
                const request = {
                    customerId: selectedCustomer.customerId,
                    username: getUsernameResponse['username'],
                    enabled: updateEnabledResponse['updatedEnabled'],
                    groups: updateGroupsResponse['updatedGroups'],
                    userAttributes: updateAttResponse['updatedAtt']
                };
                const updatedUser = yield updateUser_1.default(request);
            }
            catch (err) {
                console.log(err);
            }
        })
    };
});
exports.default = getMenuItems;
