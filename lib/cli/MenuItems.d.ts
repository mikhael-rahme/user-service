import { CLIMenuItems } from 'toolkit/lib/cli/CLIMenuItems';
declare const getMenuItems: (caller: string, stage: string) => Promise<CLIMenuItems>;
export default getMenuItems;
