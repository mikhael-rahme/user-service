import createClient from './lib/client';
import {
  authorizeUser,
  validateAccessToken,
  respondToNewPasswordRequired,
  reauthorize,
  parseTokens,
} from './lib/auth';
export * from './lib/Types';
import * as tests from './lib/tests';

export default createClient;
export {
  authorizeUser,
  validateAccessToken,
  respondToNewPasswordRequired,
  reauthorize,
  parseTokens,
  tests,
};
