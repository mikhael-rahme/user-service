const createClient = require('./lib/client').default;
const auth = require('./lib/auth');
const tests = require('./lib/tests');
const getUserServiceMenuItems = require('./lib/cli/MenuItems').default;

exports.default = createClient;
exports.authorizeUser = auth.authorizeUser;
exports.reauthorize = auth.reauthorize;
exports.respondToNewPasswordRequired = auth.respondToNewPasswordRequired;
exports.validateAccessToken = auth.validateAccessToken;
exports.parseTokens = auth.parseTokens;
exports.tests = tests;
exports.getUserServiceMenuItems = getUserServiceMenuItems;
