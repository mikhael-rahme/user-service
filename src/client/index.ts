import {
  createServiceClient,
  ServiceResult,
  ServiceRequest,
  LambdaUtils,
} from 'toolkit';
import { Lambda } from 'aws-sdk';
const {
  name: serviceName,
  version: serviceVersion,
} = require('../../package.json');
const {
  createLambdaRequest,
} = LambdaUtils;

import {
  GetUserRequest,
  GetUserResponse,
  CreateUserRequest,
  CreateUserResponse,
  ListUsersRequest,
  ListUsersResponse,
  DeleteUserRequest,
  DeleteUserResponse,
  UpdateUserRequest,
  UpdateUserResponse,
  ListGroupsRequest,
  ListGroupsResponse,
  CreateGroupRequest,
  CreateGroupResponse,
  DeleteGroupRequest,
  DeleteGroupResponse,
  AddUserToGroupRequest,
  AddUserToGroupResponse,
  RemoveUserFromGroupRequest,
  RemoveUserFromGroupResponse,
  ListUsersInGroupRequest,
  ListUsersInGroupResponse,
  ChangeUserPasswordRequest,
  ChangeUserPasswordResponse,
} from '../Types';

const createClient = (
  caller: string,
  stage: string,
  options: {
    serviceRequest?: ServiceRequest,
    version?: string,
  } = {}) => {
  const {
    serviceRequest,
    version,
  } = options;

  const lambda = new Lambda({
    apiVersion: '2015-03-31',
    region: process.env.AWS_REGION || 'us-east-1',
  });

  const createLambdaRequestArguments = (functionName: string) => ({
    lambda,
    stage,
    serviceName,
    serviceRequest,
    caller,
    functionName,
  });

  const createLambdaRequestOptions = <T>(request: T) => ({
    serviceRequest,
    version: (version || serviceVersion).replace(/\./g, '-'),
    payload: request,
  });

  return {
    getUser:
      (request: GetUserRequest): Promise<ServiceResult<GetUserResponse>> =>
        createLambdaRequest<GetUserRequest, GetUserResponse>(createLambdaRequestArguments('getUser'),
         createLambdaRequestOptions<GetUserRequest>(request)),
    listUsers:
      (request: ListUsersRequest): Promise<ServiceResult<ListUsersResponse>> =>
        createLambdaRequest<ListUsersRequest, ListUsersResponse>(createLambdaRequestArguments('listUsers'),
         createLambdaRequestOptions(request)),
    createUser: (request: CreateUserRequest): Promise<ServiceResult<CreateUserResponse>> =>
        createLambdaRequest<CreateUserRequest, CreateUserResponse>(createLambdaRequestArguments('createUser'),
         createLambdaRequestOptions(request)),
    deleteUser: (request: DeleteUserRequest): Promise<ServiceResult<DeleteUserResponse>> =>
        createLambdaRequest<DeleteUserRequest, DeleteUserResponse>(createLambdaRequestArguments('deleteUser'),
         createLambdaRequestOptions(request)),
    updateUser: (request: UpdateUserRequest): Promise<ServiceResult<UpdateUserResponse>> =>
        createLambdaRequest<UpdateUserRequest, UpdateUserResponse>(createLambdaRequestArguments('updateUser'),
         createLambdaRequestOptions(request)),
    listGroups:
      (request: ListGroupsRequest): Promise<ServiceResult<ListGroupsResponse>> =>
        createLambdaRequest<ListGroupsRequest, ListGroupsResponse>(createLambdaRequestArguments('listGroups'),
         createLambdaRequestOptions(request)),
    createGroup: (request: CreateGroupRequest): Promise<ServiceResult<CreateGroupResponse>> =>
        createLambdaRequest<CreateGroupRequest, CreateGroupResponse>(createLambdaRequestArguments('createGroup'),
         createLambdaRequestOptions(request)),
    deleteGroup: (request: DeleteGroupRequest): Promise<ServiceResult<DeleteGroupResponse>> =>
        createLambdaRequest<DeleteGroupRequest, DeleteGroupResponse>(createLambdaRequestArguments('deleteGroup'),
         createLambdaRequestOptions(request)),
    addUserToGroup: (request: AddUserToGroupRequest): Promise<ServiceResult<AddUserToGroupResponse>> =>
        createLambdaRequest<AddUserToGroupRequest, AddUserToGroupResponse>(createLambdaRequestArguments('addUserToGroup'),
         createLambdaRequestOptions(request)),
    removeUserFromGroup: (request: RemoveUserFromGroupRequest): Promise<ServiceResult<RemoveUserFromGroupResponse>> =>
        createLambdaRequest<RemoveUserFromGroupRequest, RemoveUserFromGroupResponse>(createLambdaRequestArguments('removeUserFromGroup'),
         createLambdaRequestOptions(request)),
    listUsersInGroup: (request: ListUsersInGroupRequest): Promise<ServiceResult<ListUsersInGroupResponse>> =>
        createLambdaRequest<ListUsersInGroupRequest, ListUsersInGroupResponse>(createLambdaRequestArguments('listUsersInGroup'),
         createLambdaRequestOptions(request)),
    changeUserPassword: (request: ChangeUserPasswordRequest): Promise<ServiceResult<ChangeUserPasswordResponse>> =>
        createLambdaRequest<ChangeUserPasswordRequest, ChangeUserPasswordResponse>(createLambdaRequestArguments('changeUserPassword'),
        createLambdaRequestOptions(request)),
  };
};

export default createClient;
