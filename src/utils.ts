import { CognitoIdentityServiceProvider as Cog } from 'aws-sdk';
import { testUtils, ServiceRequest, ServiceResult } from 'toolkit';
import createClient from './client';

// not null, not undefined
export const nnnu = (value: any) => value !== null && value !== undefined;

export const findDeepAndReplaceAllPropertyValue = (object: any, propertyName: string, value: any) => {

  if (typeof object === 'object' && object !== null) {
    if (Array.isArray(object)) {
      return object.map(item => findDeepAndReplaceAllPropertyValue(item, propertyName, value));
    }
    const newObject = {};
    Object.keys(object).forEach((objectKey) => {
      if (objectKey === propertyName) {
        newObject[objectKey] = value;
      } else {
        newObject[objectKey] = findDeepAndReplaceAllPropertyValue(object[objectKey], propertyName, value);
      }
    });
    return newObject;
  }

  return object;
};

export const findDeepAndAssign = (object: any, propertyName: string, value: any) => {

  if (typeof object === 'object' && object !== null) {
    if (Array.isArray(object)) {
      return object.forEach(item => findDeepAndAssign(item, propertyName, value));
    }
    Object.keys(object).forEach((objectKey) => {
      if (objectKey === propertyName) {
        object[objectKey] = value;
      } else {
        object[objectKey] = findDeepAndAssign(object[objectKey], propertyName, value);
      }
    });
  }

  return object;
};

export const client = createClient('userServiceIntegrationTests', testUtils.getTestStage());
