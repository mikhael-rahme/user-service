import {
  CreateUserRequest, CreateUserResponse,
  GetUserRequest, GetUserResponse,
  CreateUserTestObject,
  UserServiceLambdaTestObject,
} from '../../Types';
import {
  AuthorizeUserResponse,
} from '../../auth/authorizeUser/Types';

export type ChangeUserPasswordRequest = {
  accessToken: string;
  oldPassword: string;
  newPassword: string;
};
export type ChangeUserPasswordResponse = {
  error?: string;
  message?: string;
};

export interface ChangeUserPasswordTestObject extends UserServiceLambdaTestObject {
  createUserTestObject: CreateUserTestObject;
  changeUserPasswordRequest: ChangeUserPasswordRequest;
  changeUserPasswordResponse: ChangeUserPasswordResponse;
  postChangeUserPasswordAuthorizeUserResponse: AuthorizeUserResponse;
  getUserRequest: GetUserRequest;
  getUserResponse: GetUserResponse;
  username: string;
  error: Error;
}
