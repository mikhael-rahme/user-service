const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { client } from '../../utils';
import { ChangeUserPasswordTestObject } from './Types';
import changeUserPassword from './';
import createUser from '../createUser';
import getUser from '../getUser';
import { authorizeUser } from '../../auth';

import * as CreateUserTests from '../createUser/createUserTests';

const newPassword = 'someNewPassword123@#$';

const createTestObject = (): ChangeUserPasswordTestObject => ({
  environment: null,
  createUserTestObject: CreateUserTests.createTestObject(),
  changeUserPasswordRequest: null,
  changeUserPasswordResponse: null,
  postChangeUserPasswordAuthorizeUserResponse: null,
  getUserRequest: null,
  getUserResponse: null,
  username: `testUsername-${Date.now()}`,
  error: null,
});

const before00Setup = async function (testObject: ChangeUserPasswordTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);
  } catch (err) {
    console.log(err, JSON.stringify(testObject, null, 2));
    testObject.error = err;
  }
  const authorizeUserResponse = await authorizeUser({
    customerId: testObject.createUserTestObject.createUserRequest.customerId,
    clientId: testObject.createUserTestObject.createUserRequest.clientId,
    username: testObject.createUserTestObject.createUserRequest.username,
    password: testObject.createUserTestObject.createUserRequest.password,
  });
  testObject.changeUserPasswordRequest = {
    newPassword,
    accessToken: authorizeUserResponse.authenticationResponse.accessToken,
    oldPassword: testObject.createUserTestObject.password,
  }
  return testObject;
};

const before01Run = async function (testObject: ChangeUserPasswordTestObject) {
  try {
    testObject.changeUserPasswordResponse = await changeUserPassword(testObject.changeUserPasswordRequest);
  } catch (err) {
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: ChangeUserPasswordTestObject) => {
  try {
    const response = await client.changeUserPassword(testObject.changeUserPasswordRequest);
    testObject.changeUserPasswordResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: ChangeUserPasswordTestObject) => {
  testObject.postChangeUserPasswordAuthorizeUserResponse = await authorizeUser({
    customerId: testObject.createUserTestObject.createUserRequest.customerId,
    clientId: testObject.createUserTestObject.createUserRequest.clientId,
    username: testObject.createUserTestObject.createUserRequest.username,
    password: newPassword,
  });
  return testObject;
};

const after00TearDown = async (testObject: ChangeUserPasswordTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
  return testObject;
};

const test00 = (testObject: ChangeUserPasswordTestObject) => {
  describe('changetUserPassword', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('changeUserPasswordResponse return a success message', () => {
      expect(testObject.changeUserPasswordResponse).to.deep.equal({
        message: 'Success',
      });
    });
    it('should allow users to login with the new password', () => {
      expect(testObject.postChangeUserPasswordAuthorizeUserResponse.authenticationResponse).to.have.include.keys(['accessToken']);
    });
  });
  return testObject;
};

export {
  before00Setup,
  run00Integration,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
