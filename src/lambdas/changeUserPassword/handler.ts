import {
  Context,
} from 'aws-lambda';
import changeUserPassword from './index';
import {
  ChangeUserPasswordRequest,
  ChangeUserPasswordResponse,
} from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: ChangeUserPasswordRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: ChangeUserPasswordResponse = await changeUserPassword(request);
    return new ServiceResult(serviceRequest, { data });
  });
