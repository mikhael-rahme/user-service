import { CognitoIdentityServiceProvider, AWSError } from 'aws-sdk';
import { ChangeUserPasswordRequest, ChangeUserPasswordResponse } from './Types';

const changeUserPassword = async (request: ChangeUserPasswordRequest): Promise<ChangeUserPasswordResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const changeUserPasswordRequest: CognitoIdentityServiceProvider.Types.ChangePasswordRequest = {
    AccessToken: request.accessToken,
    PreviousPassword: request.oldPassword,
    ProposedPassword: request.newPassword,
  };
  try {
    const result = await client.changePassword(changeUserPasswordRequest).promise();
    return { message: 'Success' };
  } catch (err) {
    return { error: err.message };
  }
};

export default changeUserPassword;
