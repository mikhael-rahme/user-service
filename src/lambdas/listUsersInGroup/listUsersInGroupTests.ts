const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { ListUsersInGroupTestObject } from '../../Types';
import listUsersInGroup from './';

import * as AddUserToGroupTests from '../addUserToGroup/addUserToGroupTests';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;


const createTestObject = (): ListUsersInGroupTestObject => ({
  environment: null,
  addUserToGroupTestObject: AddUserToGroupTests.createTestObject(),
  listUsersInGroupRequest: null,
  listUsersInGroupResponse: null,
  error: null,
});

const before00Setup = async function (testObject: ListUsersInGroupTestObject) {
  try {
    await AddUserToGroupTests.before00Setup(testObject.addUserToGroupTestObject);
    await AddUserToGroupTests.before01Run(testObject.addUserToGroupTestObject);
    await AddUserToGroupTests.before02CollectResults(testObject.addUserToGroupTestObject);

    testObject.listUsersInGroupRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
      groupName: testObject
        .addUserToGroupTestObject
        .createGroupTestObject
        .groupName,
    };
  } catch (err) {
    console.log(err, JSON.stringify(testObject, null, 2));
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: ListUsersInGroupTestObject) {
  try {
    testObject.listUsersInGroupResponse = await listUsersInGroup(testObject.listUsersInGroupRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: ListUsersInGroupTestObject) => {
  try {
    const response = await client.listUsersInGroup(testObject.listUsersInGroupRequest);
    testObject.listUsersInGroupResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: ListUsersInGroupTestObject) => {
  // nada
  return testObject;
};

const after00TearDown = async (testObject: ListUsersInGroupTestObject) => {
  await AddUserToGroupTests.after00TearDown(testObject.addUserToGroupTestObject);
  return testObject;
};

const test00 = (testObject: ListUsersInGroupTestObject) => {
  describe('listUsersInGroup', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('listUsersInGroupResponse to include createUserResponse object', () => {
      const o0 = {
        username: testObject
          .addUserToGroupTestObject
          .createUserTestObject
          .createUserResponse.username,
        status: testObject
          .addUserToGroupTestObject
          .createUserTestObject
          .createUserResponse.status,
        enabled: testObject
          .addUserToGroupTestObject
          .createUserTestObject
          .createUserResponse.enabled,
      };

      const o1s = testObject.listUsersInGroupResponse.users;

      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
