import { User, AddUserToGroupTestObject, UserServiceLambdaTestObject } from '../../Types';

export type ListUsersInGroupRequest = {
  customerId: string;
  groupName: string;
  limit?: number;
  paginationToken?: string;
};
export type ListUsersInGroupResponse = {
  users: User[];
  paginationToken?: string;
};

export interface ListUsersInGroupTestObject extends UserServiceLambdaTestObject {
  addUserToGroupTestObject: AddUserToGroupTestObject;
  listUsersInGroupRequest: ListUsersInGroupRequest;
  listUsersInGroupResponse: ListUsersInGroupResponse;
  error: Error;
}

