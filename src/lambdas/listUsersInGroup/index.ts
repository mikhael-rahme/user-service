import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { ListUsersInGroupRequest, ListUsersInGroupResponse } from './Types';
import { cognitoUserToUser } from '../../Types';

const listUsersInGroup = async (request: ListUsersInGroupRequest): Promise<ListUsersInGroupResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const listUsersInGroupRequest: CognitoIdentityServiceProvider.Types.ListUsersInGroupRequest = {
    UserPoolId: request.customerId,
    GroupName: request.groupName,
    NextToken: request.paginationToken,
    Limit: request.limit,
  };

  const cognitoResponse: CognitoIdentityServiceProvider.Types.ListUsersInGroupResponse =
    await client.listUsersInGroup(listUsersInGroupRequest).promise();

  return {
    users: cognitoResponse.Users.map(cognitoUserToUser),
    paginationToken: cognitoResponse.NextToken,
  };
};

export default listUsersInGroup;
