import {
  Context,
} from 'aws-lambda';
import listUsersInGroup from './index';
import { ListUsersInGroupRequest, ListUsersInGroupResponse } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: ListUsersInGroupRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {
      
    const data: ListUsersInGroupResponse = await listUsersInGroup(request);
    return new ServiceResult(serviceRequest, { data });
  });
