const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { RemoveUserFromGroupTestObject } from '../../Types';
import removeUserFromGroup from './';
import listUsersInGroup from '../listUsersInGroup';

import * as AddUserToGroupTests from '../addUserToGroup/addUserToGroupTests';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;


const createTestObject = (): RemoveUserFromGroupTestObject => ({
  environment: null,
  addUserToGroupTestObject: AddUserToGroupTests.createTestObject(),
  removeUserFromGroupRequest: null,
  removeUserFromGroupResponse: null,
  listUsersInGroupRequest: null,
  listUsersInGroupResponse: null,
  error: null,
});

const before00Setup = async function (testObject: RemoveUserFromGroupTestObject) {
  try {
    await AddUserToGroupTests.before00Setup(testObject.addUserToGroupTestObject);
    await AddUserToGroupTests.before01Run(testObject.addUserToGroupTestObject);
    await AddUserToGroupTests.before02CollectResults(testObject.addUserToGroupTestObject);

    testObject.removeUserFromGroupRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
      groupName: testObject
        .addUserToGroupTestObject
        .createGroupTestObject
        .groupName,
      username: testObject
        .addUserToGroupTestObject
        .createUserTestObject
        .username,
    };
    testObject.listUsersInGroupRequest = testObject.addUserToGroupTestObject.listUsersInGroupRequest;
  } catch (err) {
    console.log(err, JSON.stringify(testObject, null, 2));
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: RemoveUserFromGroupTestObject) {
  try {
    testObject.removeUserFromGroupResponse = await removeUserFromGroup(testObject.removeUserFromGroupRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: RemoveUserFromGroupTestObject) => {
  try {
    const response = await client.removeUserFromGroup(testObject.removeUserFromGroupRequest);
    testObject.removeUserFromGroupResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: RemoveUserFromGroupTestObject) => {
  try {
    testObject.listUsersInGroupResponse = await listUsersInGroup(testObject.listUsersInGroupRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: RemoveUserFromGroupTestObject) => {
  await AddUserToGroupTests.after00TearDown(testObject.addUserToGroupTestObject);
  return testObject;
};

const test00 = (testObject: RemoveUserFromGroupTestObject) => {
  describe('removeUserFromGroup', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });  
    it('listUsersInGroupResponse0 to include createUserResponse object', () => {
      const { username, status, enabled } = testObject
        .addUserToGroupTestObject
        .createUserTestObject
        .createUserResponse;
      const o0 = { username, status, enabled };
      const o1s = testObject.addUserToGroupTestObject.listUsersInGroupResponse.users;

      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
    it('removeUserFromGroupResponse to equal removeUserFromGroupRequest', () => {
      expect(testObject.removeUserFromGroupResponse).to.deep.equal({
        username: testObject.removeUserFromGroupRequest.username,
        groupName: testObject.removeUserFromGroupRequest.groupName,
      });
    });
    it('listUsersInGroupResponse1 to not return users', () => {
      const { username, status, enabled } = testObject
        .addUserToGroupTestObject
        .createUserTestObject
        .createUserResponse;
      const o0 = { username, status, enabled };
      const o1s = testObject.listUsersInGroupResponse.users;

      expect(o1s.some(o1 => subsetOf(o0, o1))).false;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
