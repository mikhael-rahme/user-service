import {
  ListUsersInGroupRequest, ListUsersInGroupResponse,
  RemoveUserFromGroupRequest, RemoveUserFromGroupResponse,
  AddUserToGroupTestObject,
  UserServiceLambdaTestObject,
} from '../../Types';

export type RemoveUserFromGroupRequest = {
  customerId: string;
  username: string;
  groupName: string;
};
export type RemoveUserFromGroupResponse = {
  username: string;
  groupName: string;
};

export interface RemoveUserFromGroupTestObject extends UserServiceLambdaTestObject {
  addUserToGroupTestObject: AddUserToGroupTestObject;
  removeUserFromGroupRequest: RemoveUserFromGroupRequest;
  removeUserFromGroupResponse: RemoveUserFromGroupResponse;
  listUsersInGroupRequest: ListUsersInGroupRequest;
  listUsersInGroupResponse: ListUsersInGroupResponse;
  error: Error;
}
