import {
  Context,
} from 'aws-lambda';
import removeUserFromGroup from './index';
import { RemoveUserFromGroupRequest, RemoveUserFromGroupResponse } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: RemoveUserFromGroupRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: RemoveUserFromGroupResponse = await removeUserFromGroup(request);
    return new ServiceResult(serviceRequest, { data });
  });
