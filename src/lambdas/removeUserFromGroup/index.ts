import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { RemoveUserFromGroupRequest, RemoveUserFromGroupResponse } from './Types';

const removeUserFromGroup = async (request: RemoveUserFromGroupRequest): Promise<RemoveUserFromGroupResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const removeUserFromGroupRequest: CognitoIdentityServiceProvider.Types.AdminRemoveUserFromGroupRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
    GroupName: request.groupName,
  };
  await client.adminRemoveUserFromGroup(removeUserFromGroupRequest).promise();

  return {
    username: request.username,
    groupName: request.groupName,
  };
};

export default removeUserFromGroup;
