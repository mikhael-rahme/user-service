import {
  Context,
} from 'aws-lambda';
import deleteGroup from './index';
import { DeleteGroupRequest, DeleteGroupResponse } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: DeleteGroupRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: DeleteGroupResponse = await deleteGroup(request);
    return new ServiceResult(serviceRequest, { data });
  });
