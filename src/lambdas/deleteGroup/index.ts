import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { DeleteGroupRequest, DeleteGroupResponse } from './Types';

const deleteGroup = async (request: DeleteGroupRequest): Promise<DeleteGroupResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const deleteGroupRequest: CognitoIdentityServiceProvider.Types.DeleteGroupRequest = {
    UserPoolId: request.customerId,
    GroupName: request.groupName,
  };

  await client.deleteGroup(deleteGroupRequest).promise();

  return {
    groupName: request.groupName,
  };
};

export default deleteGroup;
