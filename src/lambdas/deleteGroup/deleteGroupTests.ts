const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { DeleteGroupTestObject } from '../../Types';
import deleteGroup from './';
import listGroups from '../listGroups';

import * as CreateGroupTests from '../createGroup/createGroupTests';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;


const createTestObject = (): DeleteGroupTestObject => ({
  environment: null,
  createGroupTestObject: CreateGroupTests.createTestObject(),
  deleteGroupRequest: null,
  deleteGroupResponse: null,
  listGroupsRequest: null,
  listGroupsResponse: null,
  error: null,
});

const before00Setup = async function (testObject: DeleteGroupTestObject) {
  try {
    await CreateGroupTests.before00Setup(testObject.createGroupTestObject);
    await CreateGroupTests.before01Run(testObject.createGroupTestObject);
    await CreateGroupTests.before02CollectResults(testObject.createGroupTestObject);

    testObject.listGroupsRequest = testObject.createGroupTestObject.createGroupRequest;
    testObject.deleteGroupRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
      groupName: testObject
        .createGroupTestObject.groupName,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: DeleteGroupTestObject) {
  try {
    testObject.deleteGroupResponse = await deleteGroup(testObject.deleteGroupRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: DeleteGroupTestObject) => {
  try {
    const response = await client.deleteGroup(testObject.deleteGroupRequest);
    testObject.deleteGroupResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: DeleteGroupTestObject) => {
  try {
    testObject.listGroupsResponse = await listGroups(testObject.listGroupsRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: DeleteGroupTestObject) => {
  await CreateGroupTests.after00TearDown(testObject.createGroupTestObject);
  return testObject;
};

const test00 = (testObject: DeleteGroupTestObject) => {
  describe('deleteGroup', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('deleteGroupResponse to deep include deleteGroupRequest', () => {
      expect(testObject.deleteGroupResponse).to.deep.equal({
        groupName: testObject.deleteGroupRequest.groupName,
      });
    });
    it('listUsersResponse to not include groupName', () => {
      const o0 = {
        groupName: testObject.deleteGroupRequest.groupName,
      };
      const o1s = testObject.listGroupsResponse.groups;

      expect(o1s.some(o1 => subsetOf(o0, o1))).false;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
