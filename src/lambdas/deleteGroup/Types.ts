import { CreateGroupTestObject, ListGroupsRequest, ListGroupsResponse, UserServiceLambdaTestObject } from '../../Types';

export type DeleteGroupRequest = {
  customerId: string;
  groupName: string;
};
export type DeleteGroupResponse = {
  groupName: string;
};

export interface DeleteGroupTestObject extends UserServiceLambdaTestObject {
  createGroupTestObject: CreateGroupTestObject;
  deleteGroupRequest: DeleteGroupRequest;
  deleteGroupResponse: DeleteGroupResponse;
  listGroupsRequest: ListGroupsRequest;
  listGroupsResponse: ListGroupsResponse;
  error: Error;
}
