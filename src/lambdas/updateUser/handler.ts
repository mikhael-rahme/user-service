import {
  Context,
} from 'aws-lambda';
import updateUser from './index';
import { UpdateUserRequest, UpdateUserResponse } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: UpdateUserRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: UpdateUserResponse = await updateUser(request);
    return new ServiceResult(serviceRequest, { data });
  });
