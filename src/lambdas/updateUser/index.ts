import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { UpdateUserRequest, UpdateUserResponse, cognitoUserAttributeToAttribute, cognitoGroupToGroup } from '../../Types';

const updateUser = async (request: UpdateUserRequest): Promise<UpdateUserResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });
  const getUserRequest: CognitoIdentityServiceProvider.Types.AdminGetUserRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
  };
  const listGroupsForUser: CognitoIdentityServiceProvider.Types.AdminListGroupsForUserRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
  };

  const userData = await Promise.all([
    client.adminGetUser(getUserRequest).promise(),
    client.adminListGroupsForUser(listGroupsForUser).promise(),
  ]);

  const updateUserAttributesRequest: CognitoIdentityServiceProvider.Types.AdminUpdateUserAttributesRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
    UserAttributes: request.userAttributes.map(att => ({ Name: att.name, Value: att.value })),
  };

  // checkGroups
  const addGroups = request.groups.filter(g => !userData[1].Groups.some(G => g.groupName === G.GroupName));
  const removeGroups = userData[1].Groups.filter(G => !request.groups.some(g => g.groupName === G.GroupName));
  // Add all to an array so that they an be wrapped in a Promise.all
  const combinedOperations: any = addGroups
    .map(g => client.adminAddUserToGroup({ UserPoolId: request.customerId, Username: request.username, GroupName: g.groupName }).promise())
      .concat(removeGroups.map(G =>
        client.adminRemoveUserFromGroup({ UserPoolId: request.customerId, Username: request.username, GroupName: G.GroupName }).promise()));
  combinedOperations.push(client.adminUpdateUserAttributes(updateUserAttributesRequest).promise());
  if (userData[0].Enabled !== request.enabled) {
    combinedOperations.push(request.enabled ?
      client.adminEnableUser({ UserPoolId: request.customerId, Username: request.username }).promise() :
      client.adminDisableUser({ UserPoolId: request.customerId, Username: request.username }).promise(),
    );
  }

  const res = await Promise.all(combinedOperations);

  return {
    username: request.username,
    enabled: request.enabled !== undefined ? request.enabled : userData[0].Enabled,
    userAttributes: request.userAttributes !== undefined ? request.userAttributes :
      userData[0].UserAttributes.map(cognitoUserAttributeToAttribute),
    groups: request.groups !== undefined ? request.groups : userData[1].Groups.map(cognitoGroupToGroup),
  };
};

export default updateUser;
