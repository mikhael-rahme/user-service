const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { UpdateUserTestObject } from '../../Types';
import createGroup from '../createGroup';
import * as AddUserToGroupTests from '../addUserToGroup/addUserToGroupTests';
import getUser from '../getUser';
import updateUser from './';

const createTestObject = (): UpdateUserTestObject => ({
  environment: null,
  addUserToGroupTestObject: AddUserToGroupTests.createTestObject(),
  createGroupRequest: null,
  createGroupResponse: null,
  updateUserRequest: null,
  updateUserResponse: null,
  getUserRequest: null,
  getUserResponse: null,
  newEmail: `testEmail0@test.com`,
  error: null,
});

const before00Setup = async function (testObject: UpdateUserTestObject) {
  try {
    await AddUserToGroupTests.before00Setup(testObject.addUserToGroupTestObject);
    await AddUserToGroupTests.before01Run(testObject.addUserToGroupTestObject);
    await AddUserToGroupTests.before02CollectResults(testObject.addUserToGroupTestObject);

    const customerId = testObject.environment.createCustomerResponse.customerId;
    const username = testObject.addUserToGroupTestObject.createUserTestObject.username;

    testObject.createGroupRequest = { customerId, groupName: 'addGroup' };
    testObject.createGroupResponse = await createGroup(testObject.createGroupRequest);

    testObject.updateUserRequest = {
      customerId,
      username,
      groups: [
        { groupName: 'addGroup' },
      ],
      enabled: false,
      userAttributes: [
        { name: 'email', value: testObject.newEmail },
      ],
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: UpdateUserTestObject) {
  try {
    testObject.updateUserResponse = await updateUser(testObject.updateUserRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: UpdateUserTestObject) => {
  try {
    const response = await client.updateUser(testObject.updateUserRequest);
    testObject.updateUserResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: UpdateUserTestObject) => {
  try {
    const customerId = testObject.environment.createCustomerResponse.customerId;
    const username = testObject.addUserToGroupTestObject.createUserTestObject.username;

    testObject.getUserRequest = {
      customerId,
      username,
    };
    testObject.getUserResponse = await getUser(testObject.getUserRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: UpdateUserTestObject) => {
  await AddUserToGroupTests.after00TearDown(testObject.addUserToGroupTestObject);
  return testObject;
};

const test00 = (testObject: UpdateUserTestObject) => {
  describe('updateUser', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('getUserResponse.userAttributes to include second email', () => {
      expect(testObject.getUserResponse.userAttributes).to.deep.include(testObject.updateUserRequest.userAttributes[0]);
    });
    it(`getUserResponse.groups to include addGroup and not ${testObject.addUserToGroupTestObject.createGroupTestObject.groupName}`, () => {
      expect(testObject.updateUserResponse.groups.length).to.equal(1);
      expect(testObject.updateUserResponse.groups[0]).to.deep.include({ groupName: testObject.createGroupRequest.groupName });
    });
    it('getUserResponse.enabled to be false', () => {
      expect(testObject.updateUserResponse.enabled).false;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
