const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { CreateCustomerRequest, CreateCustomerResponse } from 'customer-service';
import createCustomer from 'customer-service/lib/lambdas/createCustomer';
import { ForgotPasswordTestObject } from '../../Types';
import forgotPassword from './';
import getUser from '../getUser';

import * as CreateUserTests from '../createUser/createUserTests';



const createTestObject = (): ForgotPasswordTestObject => ({
  createUserTestObject: CreateUserTests.createTestObject(),
  forgotPasswordRequest: null,
  forgotPasswordResponse: null,
  getUserRequest: null,
  getUserResponse: null,
  error: null,
});

const before00Setup = async function (testObject: ForgotPasswordTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);
    const { clientId, customerId } = testObject.createUserTestObject.createCustomerTestObject.createCustomerResponse;
    const username = testObject.createUserTestObject.username;
    testObject.forgotPasswordRequest = {
      clientId,
      username,
    };
    testObject.getUserRequest = {
      customerId,
      username,
    };
  } catch (err) {
    console.log(err, JSON.stringify(testObject, null, 2));
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: ForgotPasswordTestObject) {
  try {
    testObject.forgotPasswordResponse = await forgotPassword(testObject.forgotPasswordRequest);
  } catch (err) {
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: ForgotPasswordTestObject) => {
  try {
    testObject.getUserResponse = await getUser(testObject.getUserRequest);
  } catch (err) {
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: ForgotPasswordTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
  return testObject;
};

const test00 = (testObject: ForgotPasswordTestObject) => {
  describe('forgotPassword', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('forgotPassword should consist of given username', () => {
      expect(testObject.forgotPasswordResponse).to.deep.equal({
        username: testObject.createUserTestObject.username,
      });
    });
    it('user status now equals ""', () => {
      console.log('testObject.getUserResponse:', JSON.stringify(testObject.getUserResponse, null, 2));
      expect(testObject.getUserResponse.status).to.equal('');
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
