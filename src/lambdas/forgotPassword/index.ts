import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { ForgotPasswordRequest, ForgotPasswordResponse } from './Types';

const forgotPassword = async (request: ForgotPasswordRequest): Promise<ForgotPasswordResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const forgotPasswordRequest: CognitoIdentityServiceProvider.Types.ForgotPasswordRequest = {
    ClientId: request.clientId,
    Username: request.username,
  };

  await client.forgotPassword(forgotPasswordRequest).promise();
  return {
    username: request.username,
  };
};

export default forgotPassword;
