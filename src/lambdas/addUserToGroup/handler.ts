import {
  Context,
} from 'aws-lambda';
import addUserToGroup from './index';
import { AddUserToGroupResponse, AddUserToGroupRequest } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: AddUserToGroupRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: AddUserToGroupResponse = await addUserToGroup(request);
    return new ServiceResult(serviceRequest, { data });
  });
