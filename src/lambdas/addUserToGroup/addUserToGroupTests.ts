const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { AddUserToGroupTestObject } from '../../Types';

import listUsersInGroup from '../listUsersInGroup';
import addUserToGroup from './';
import createUser from '../createUser';

import * as CreateGroupTests from '../createGroup/createGroupTests';
import * as CreateUserTests from '../createUser/createUserTests';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;


const createTestObject = (): AddUserToGroupTestObject => ({
  environment: null,
  createGroupTestObject: CreateGroupTests.createTestObject(),
  createUserTestObject: CreateUserTests.createTestObject(),
  addUserToGroupRequest: null,
  addUserToGroupResponse: null,
  listUsersInGroupRequest: null,
  listUsersInGroupResponse: null,
  error: null,
});

const before00Setup = async function (testObject: AddUserToGroupTestObject) {
  try {
    await CreateGroupTests.before00Setup(testObject.createGroupTestObject);
    await CreateGroupTests.before01Run(testObject.createGroupTestObject);
    await CreateGroupTests.before02CollectResults(testObject.createGroupTestObject);

    const { groupName } = testObject.createGroupTestObject;
    const { username, password } = testObject.createUserTestObject;
    const { clientId, customerId } = testObject.environment.createCustomerResponse;

    testObject.createUserTestObject.createUserRequest = {
      username,
      password,
      customerId,
      clientId,
    };
    testObject.createUserTestObject.createUserResponse = await createUser(testObject.createUserTestObject.createUserRequest);

    testObject.addUserToGroupRequest = {
      customerId,
      username,
      groupName,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }

  return testObject;
};

const before01Run = async function (testObject: AddUserToGroupTestObject) {
  try {
    testObject.addUserToGroupResponse = await addUserToGroup(testObject.addUserToGroupRequest);
  } catch (err) {
    testObject.error = err;
  }

  return testObject;
};

const run00Integration = async (testObject: AddUserToGroupTestObject) => {
  try {
    const response = await client.addUserToGroup(testObject.addUserToGroupRequest);
    testObject.addUserToGroupResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before02CollectResults = async (testObject: AddUserToGroupTestObject) => {
  try {
    testObject.listUsersInGroupRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse
        .customerId,
      groupName: testObject
        .createGroupTestObject
        .groupName,
    };
    testObject.listUsersInGroupResponse = await listUsersInGroup(testObject.listUsersInGroupRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: AddUserToGroupTestObject) => {
  await CreateGroupTests.after00TearDown(testObject.createGroupTestObject);
  return testObject;
};

const test00 = (testObject: AddUserToGroupTestObject) => {
  describe('addUserToGroup', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('addUserToGroupResponse to include username and groupName', () => {
      expect(testObject.addUserToGroupResponse).to.deep.equal({
        username: testObject
          .createUserTestObject
          .username,
        groupName: testObject
          .createGroupTestObject
          .groupName,
      });
    });
    it('listUsersInGroup to include testUser', () => {
      const o0 = {
        username: testObject
          .createUserTestObject
          .username,
        enabled: true,
      };
      const o1s = testObject.listUsersInGroupResponse.users;

      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
