import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { AddUserToGroupRequest, AddUserToGroupResponse } from './Types';

const addUserToGroup = async (request: AddUserToGroupRequest): Promise<AddUserToGroupResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const addUserToGroupRequest: CognitoIdentityServiceProvider.Types.AdminAddUserToGroupRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
    GroupName: request.groupName,
  };

  await client.adminAddUserToGroup(addUserToGroupRequest).promise();

  return {
    username: request.username,
    groupName: request.groupName,
  };
};

export default addUserToGroup;
