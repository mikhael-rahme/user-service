import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { ListUsersRequest, ListUsersResponse } from './Types';
import { cognitoUserToUser } from '../../Types';

const CogTypes = CognitoIdentityServiceProvider.Types;

const listUsers = async (request: ListUsersRequest): Promise<ListUsersResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const listUsersRequest: CognitoIdentityServiceProvider.Types.ListUsersRequest = {
    UserPoolId: request.customerId,
    Limit: request.limit,
  };

  const cognitoResponse: CognitoIdentityServiceProvider.Types.ListUsersResponse = await client.listUsers(listUsersRequest).promise();
  const cognitoGroupResponse: CognitoIdentityServiceProvider.Types.AdminListGroupsForUserResponse[] = await Promise.all(
    cognitoResponse.Users.map(user =>
      client.adminListGroupsForUser({ UserPoolId: listUsersRequest.UserPoolId, Username: user.Username }).promise()));

  return {
    users: cognitoResponse.Users.map((user, index) => cognitoUserToUser({ ...user, Groups: cognitoGroupResponse[index].Groups })),
    paginationToken: cognitoResponse.PaginationToken,
  };
};

export default listUsers;
