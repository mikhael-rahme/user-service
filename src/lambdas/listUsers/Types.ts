import { UserAttribute, User, CreateUserTestObject, UserServiceLambdaTestObject } from '../../Types';
export type ListUsersRequest = {
  customerId: string;
  limit?: number;
  paginationToken?: string;
  attributeFilters?: UserAttribute[];
};

export type ListUsersResponse = {
  users: User[];
  paginationToken?: string;
};

export interface ListUsersTestObject extends UserServiceLambdaTestObject {
  createUserTestObject: CreateUserTestObject;
  listUsersRequest: ListUsersRequest;
  listUsersResponse: ListUsersResponse;
  error: Error;
}
