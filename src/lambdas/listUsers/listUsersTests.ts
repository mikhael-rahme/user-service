const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { ListUsersTestObject } from '../../Types';
import { testUtils } from 'toolkit';
import listUsers from './';
import * as CreateUserTests from '../createUser/createUserTests';

const { subsetOf } = testUtils;

const createTestObject = (): ListUsersTestObject => ({
  environment: null,
  createUserTestObject: CreateUserTests.createTestObject(),
  listUsersRequest: null,
  listUsersResponse: null,
  error: null,
});

const before00Setup = async function (testObject: ListUsersTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);

    testObject.listUsersRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: ListUsersTestObject) {
  try {
    testObject.listUsersResponse = await listUsers(testObject.listUsersRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: ListUsersTestObject) => {
  try {
    const response = await client.listUsers(testObject.listUsersRequest);
    testObject.listUsersResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: ListUsersTestObject) => {
  // nada
  return testObject;
};

const after00TearDown = async (testObject: ListUsersTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
  return testObject;
};

const test00 = (testObject: ListUsersTestObject) => {
  describe('listUsers', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('listUsersResponse to include createUserResponse object', () => {
      const o0 = {
        username: testObject
          .createUserTestObject
          .createUserResponse.username,
        status: testObject
          .createUserTestObject
          .createUserResponse.status,
        enabled: testObject
          .createUserTestObject
          .createUserResponse.enabled,
      };
      const o1s = testObject.listUsersResponse.users;

      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
