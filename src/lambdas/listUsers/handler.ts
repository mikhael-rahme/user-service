import {
  Context,
} from 'aws-lambda';
import listUsers from './index';
import { ListUsersResponse, ListUsersRequest } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: ListUsersRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: ListUsersResponse = await listUsers(request);
    return new ServiceResult(serviceRequest, { data });
  });
