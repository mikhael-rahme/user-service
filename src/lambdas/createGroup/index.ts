import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { CreateGroupRequest, CreateGroupResponse } from './Types';
import { cognitoGroupToGroup } from '../../Types';

const createGroup = async (request: CreateGroupRequest): Promise<CreateGroupResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const createGroupRequest: CognitoIdentityServiceProvider.Types.CreateGroupRequest = {
    UserPoolId: request.customerId,
    GroupName: request.groupName,
    Description: request.description,
    Precedence: request.precedence,
    RoleArn: request.roleArn,
  };

  const cognitoResponse: CognitoIdentityServiceProvider.Types.CreateGroupResponse = await client.createGroup(createGroupRequest).promise();

  return cognitoGroupToGroup(cognitoResponse.Group);
};

export default createGroup;
