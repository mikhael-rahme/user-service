import {
  Context,
} from 'aws-lambda';
import createGroup from './index';
import { CreateGroupResponse, CreateGroupRequest } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: CreateGroupRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: CreateGroupResponse = await createGroup(request);
    return new ServiceResult(serviceRequest, { data });
  });
