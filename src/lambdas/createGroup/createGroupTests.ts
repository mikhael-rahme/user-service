const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { CreateGroupTestObject } from '../../Types';
import createGroup from '../createGroup';
import listGroups from '../listGroups';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;

const createTestObject = (): CreateGroupTestObject => ({
  environment: null,
  createGroupRequest: null,
  createGroupResponse: null,
  listGroupsRequest: null,
  listGroupsResponse: null,
  groupName: `group-${Math.ceil(Math.random() * 999999999999)}`,
  description: 'testGroupDescription',
  error: null,
});

const before00Setup = async function (testObject: CreateGroupTestObject) {
  try {
    testObject.createGroupRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
      groupName: testObject.groupName,
      description: testObject.description,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: CreateGroupTestObject) {
  try {
    testObject.createGroupResponse = await createGroup(testObject.createGroupRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: CreateGroupTestObject) => {
  try {
    const response = await client.createGroup(testObject.createGroupRequest);
    testObject.createGroupResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before02CollectResults = async (testObject: CreateGroupTestObject) => {
  try {
    testObject.listGroupsRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
    };
    testObject.listGroupsResponse = await listGroups(testObject.listGroupsRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: CreateGroupTestObject) => {
  return testObject;
};

const test00 = (testObject: CreateGroupTestObject) => {
  describe('createGroup', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('createGroupResponse to include username and groupName', () => {
      expect(testObject.createGroupResponse).to.deep.equal({
        groupName: testObject.groupName,
        description: testObject.description,
      });
    });
    it('listGroupsResponse to include testGroup', () => {
      const o0 = {
        groupName: testObject.groupName,
        description: testObject.description,
      };
      const o1s = testObject.listGroupsResponse.groups;
      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
