import { User, CreateUserTestObject, UserServiceLambdaTestObject } from '../../Types';
export type GetUserRequest = {
  customerId: string;
  username: string;
};
export type GetUserResponse = User;

export interface GetUserTestObject extends UserServiceLambdaTestObject {
  createUserTestObject: CreateUserTestObject;
  getUserRequest: GetUserRequest;
  getUserResponse: GetUserResponse;
  error: Error;
}
