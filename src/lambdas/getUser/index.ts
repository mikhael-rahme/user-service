import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { GetUserRequest, GetUserResponse } from './Types';
import { cognitoUserToUser, cognitoGroupToGroup } from '../../Types';

const getUser = async (request: GetUserRequest): Promise<GetUserResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const getUserRequest: CognitoIdentityServiceProvider.Types.AdminGetUserRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
  };

  const cognitoResponse: CognitoIdentityServiceProvider.Types.AdminGetUserResponse = await client.adminGetUser(getUserRequest).promise();
  const cognitoResponseGroups: CognitoIdentityServiceProvider.Types.AdminListGroupsForUserResponse =
    await client.adminListGroupsForUser(getUserRequest as CognitoIdentityServiceProvider.Types.AdminGetUserRequest).promise();

  return cognitoUserToUser({ ...cognitoResponse, Groups: cognitoResponseGroups.Groups });
};

export default getUser;
