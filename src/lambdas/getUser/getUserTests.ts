const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { GetUserTestObject } from '../../Types';
import { client } from '../../utils';
import getUser from './';

import * as CreateUserTests from '../createUser/createUserTests';

const createTestObject = (): GetUserTestObject => ({
  environment: null,
  createUserTestObject: CreateUserTests.createTestObject(),
  getUserRequest: null,
  getUserResponse: null,
  error: null,
});

const before00Setup = async function (testObject: GetUserTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);

    testObject.getUserRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
      username: testObject
        .createUserTestObject
        .username,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: GetUserTestObject) {
  try {
    testObject.getUserResponse = await getUser(testObject.getUserRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: GetUserTestObject) => {
  try {
    const response = await client.getUser(testObject.getUserRequest);
    testObject.getUserResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: GetUserTestObject) => {
  // nada
  return testObject;
};

const after00TearDown = async (testObject: GetUserTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
  return testObject;
};

const test00 = (testObject: GetUserTestObject) => {
  describe('getUser', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('getUserResponse to deep equal createUserResponse', () => {
      expect(JSON.parse(JSON.stringify(testObject.getUserResponse))).to.deep.include({
        username: testObject
          .createUserTestObject
          .createUserResponse.username,
        enabled: testObject
          .createUserTestObject
          .createUserResponse.enabled,
        groups: [{
          groupName: testObject
            .createUserTestObject
            .createGroupRequest.groupName,
        }],
        status: testObject
          .createUserTestObject
          .createUserResponse.status,
      });
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
