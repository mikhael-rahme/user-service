import {
  Context,
} from 'aws-lambda';
import getUser from './index';
import { GetUserResponse, GetUserRequest } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: GetUserRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: GetUserResponse = await getUser(request);
    return new ServiceResult(serviceRequest, { data });
  });
