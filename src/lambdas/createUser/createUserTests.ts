const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CreateUserTestObject } from '../../Types';
import { client } from '../../utils';
import createUser from './';
import getUser from '../getUser';
import createGroup from '../createGroup';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;

const createTestObject = (): CreateUserTestObject => ({
  environment: null,
  createUserRequest: null,
  createUserResponse: null,
  createGroupRequest: null,
  createGroupResponse: null,
  getUserRequest: null,
  getUserResponse: null,
  username: `user-${Math.ceil(Math.random() * 999999999999)}`,
  password: '1testPassword4you!',
  email: 'test@test.com',
  error: null,
});

const before00Setup = async function (testObject: CreateUserTestObject) {
  try {
    const { customerId, clientId } = testObject.environment.createCustomerResponse;
    testObject.createGroupRequest = {
      customerId,
      groupName: `group-${Math.ceil(Math.random() * 999999999999)}`,
    };

    testObject.createGroupResponse = await createGroup(testObject.createGroupRequest);

    testObject.getUserRequest = {
      customerId,
      username: testObject.username,
    };
    testObject.createUserRequest = {
      customerId,
      clientId,
      username: testObject.username,
      password: testObject.password,
      userAttributes: [
        { name: 'email', value: testObject.email },
      ],
      groups: [
        { groupName: testObject.createGroupRequest.groupName },
      ],
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: CreateUserTestObject) {
  try {
    testObject.createUserResponse = await createUser(testObject.createUserRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: CreateUserTestObject) => {
  try {
    const response = await client.createUser(testObject.createUserRequest);
    testObject.createUserResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: CreateUserTestObject) => {
  try {
    testObject.getUserResponse = await getUser(testObject.getUserRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: CreateUserTestObject) => {
  return testObject;
};

const test00 = (testObject: CreateUserTestObject) => {
  describe('createUser', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('createUserResponse to include username', () => {
      expect(testObject.createUserResponse).to.deep.include({
        username: testObject.username,
        status: 'CONFIRMED',
        enabled: true,
      });
    });
    it('createUserResponse.email to equal testObject.email', () => {
      expect(testObject.createUserResponse.userAttributes.find(x => x.name === 'email').value).to.equal(testObject.email);
    });
    it('getUserResponse to include testUser', () => {
      const o0 = {
        username: testObject.username,
        userAttributes: [
          { name: 'email', value: testObject.email },
        ],
        groups: [
          { groupName: testObject.createGroupRequest.groupName },
        ],
      };
      const o1 = testObject.getUserResponse;
      expect(subsetOf(o0, o1)).true;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
