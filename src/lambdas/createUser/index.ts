import { CognitoIdentityServiceProvider } from 'aws-sdk';
import {
  CreateUserRequest, CreateUserResponse,
  AuthorizeUserResponse,
  RespondToNewPasswordRequiredResponse,
  cognitoUserToUser,
} from '../../Types';
import authorizeUser from '../../auth/authorizeUser';
import respondToNewPasswordRequired from '../../auth/respondToNewPasswordRequired';

const createUser = async (request: CreateUserRequest): Promise<CreateUserResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const createUserRequest: CognitoIdentityServiceProvider.Types.AdminCreateUserRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
    UserAttributes: request.userAttributes ?
      request.userAttributes.map(att => ({ Name: att.name, Value: att.value })) : undefined,
    TemporaryPassword: `${request.password}1`,
  };

  if (!createUserRequest.UserAttributes || createUserRequest.UserAttributes.length === 0) {
    createUserRequest.UserAttributes = [{ Name: 'email', Value: 'noemail@iterrex.com' }];
  } else if (!createUserRequest.UserAttributes.find(x => x.Name === 'email')) {
    createUserRequest.UserAttributes.push({ Name: 'email', Value: 'noemail@iterrex.com' });
  }

  const cognitoCreateUserResponse: CognitoIdentityServiceProvider.Types.AdminCreateUserResponse =
    await client.adminCreateUser(createUserRequest).promise();

  const authorizeUserResponse: AuthorizeUserResponse = await authorizeUser({
    customerId: request.customerId,
    clientId: request.clientId,
    username: request.username,
    password: `${request.password}1`,
  });

  const respondToNewPasswordRequiredResponse: RespondToNewPasswordRequiredResponse = await respondToNewPasswordRequired({
    customerId: request.customerId,
    clientId: request.clientId,
    username: request.username,
    session: authorizeUserResponse.session,
    newPassword: request.password,
  });

  if (request.groups) {
    await Promise.all(request.groups.map(group => client.adminAddUserToGroup({
      UserPoolId: request.customerId,
      Username: request.username,
      GroupName: group.groupName,
    }).promise()));
  }

  // Manually setting to confirmed, because that'll be the status, if nothing errors out.
  cognitoCreateUserResponse.User.UserStatus = 'CONFIRMED';

  const Groups = request.groups ? request.groups.map(g => ({ GroupName: g.groupName })) : [];

  return cognitoUserToUser({ Groups, ...cognitoCreateUserResponse.User });
};

export default createUser;
