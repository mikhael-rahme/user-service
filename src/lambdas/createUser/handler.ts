import {
  Context,
} from 'aws-lambda';
import createUser from './index';
import { CreateUserResponse, CreateUserRequest } from './Types';
const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: CreateUserRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const createUserResponse: CreateUserResponse = await createUser(request);

    return new ServiceResult(serviceRequest, { data: createUserResponse });
  });
