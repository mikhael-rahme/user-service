import {
  CreateUserRequest, CreateUserResponse,
  ResetUserPasswordRequest, ResetUserPasswordResponse,
  GetUserRequest, GetUserResponse,
  CreateUserTestObject,
  UserServiceLambdaTestObject,
} from '../../Types';

export type ResetUserPasswordRequest = {
  customerId: string;
  username: string;
};
export type ResetUserPasswordResponse = {
  username: string;
};

export interface ResetUserPasswordTestObject extends UserServiceLambdaTestObject {
  createUserTestObject: CreateUserTestObject;
  resetUserPasswordRequest: ResetUserPasswordRequest;
  resetUserPasswordResponse: ResetUserPasswordResponse;
  getUserRequest: GetUserRequest;
  getUserResponse: GetUserResponse;
  username: string;
  error: Error;
}
