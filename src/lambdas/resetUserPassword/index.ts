import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { ResetUserPasswordRequest, ResetUserPasswordResponse } from './Types';

const removeUserFromGroup = async (request: ResetUserPasswordRequest): Promise<ResetUserPasswordResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const adminResetUserPasswordRequest: CognitoIdentityServiceProvider.Types.AdminResetUserPasswordRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
  };

  await client.adminResetUserPassword(adminResetUserPasswordRequest).promise();
  return {
    username: request.username,
  };
};

export default removeUserFromGroup;
