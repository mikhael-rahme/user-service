const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { client } from '../../utils';
import { ResetUserPasswordTestObject } from '../../Types';
import resetUserPassword from './';
import createUser from '../createUser';
import getUser from '../getUser';

import * as CreateUserTests from '../createUser/createUserTests';



const createTestObject = (): ResetUserPasswordTestObject => ({
  environment: null,
  createUserTestObject: CreateUserTests.createTestObject(),
  resetUserPasswordRequest: null,
  resetUserPasswordResponse: null,
  getUserRequest: null,
  getUserResponse: null,
  username: `testUsername-${Date.now()}`,
  error: null,
});

const before00Setup = async function (testObject: ResetUserPasswordTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);
  } catch (err) {
    console.log(err, JSON.stringify(testObject, null, 2));
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: ResetUserPasswordTestObject) {
  try {
    testObject.resetUserPasswordResponse = await resetUserPassword(testObject.resetUserPasswordRequest);
  } catch (err) {
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: ResetUserPasswordTestObject) => {
  try {
    testObject.getUserRequest = testObject.resetUserPasswordRequest;
    testObject.getUserResponse = await getUser(testObject.getUserRequest);
  } catch (err) {
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: ResetUserPasswordTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
  return testObject;
};

const test00 = (testObject: ResetUserPasswordTestObject) => {
  describe('resetUserPassword', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('resetUserPasswordResponse should consist of given username', () => {
      expect(testObject.resetUserPasswordResponse).to.deep.equal({
        username: testObject.username,
      });
    });
    it('user status now equals ""', () => {
      expect(testObject.getUserResponse.status).to.equal('');
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
