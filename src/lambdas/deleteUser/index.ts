import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { DeleteUserRequest, DeleteUserResponse } from './Types';

const deleteGroup = async (request: DeleteUserRequest): Promise<DeleteUserResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const deleteUserRequest: CognitoIdentityServiceProvider.Types.AdminDeleteUserRequest = {
    UserPoolId: request.customerId,
    Username: request.username,
  };

  await client.adminDeleteUser(deleteUserRequest).promise();
  
  return {
    username: request.username,
  };
};

export default deleteGroup;
