import {
  Context,
} from 'aws-lambda';
import deleteUser from './index';
import { DeleteUserResponse, DeleteUserRequest } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: DeleteUserRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: DeleteUserResponse = await deleteUser(request);
    return new ServiceResult(serviceRequest, { data });
  });
