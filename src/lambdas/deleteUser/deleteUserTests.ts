const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { client } from '../../utils';
import { DeleteUserTestObject } from '../../Types';
import deleteUser from './';
import listUsers from '../listUsers';

import * as CreateUserTests from '../createUser/createUserTests';

import { testUtils } from 'toolkit';
const { subsetOf } = testUtils;

const createTestObject = (): DeleteUserTestObject => ({
  environment: null,
  createUserTestObject: CreateUserTests.createTestObject(),
  listUsersRequest0: null,
  listUsersResponse0: null,
  deleteUserRequest: null,
  deleteUserResponse: null,
  listUsersRequest1: null,
  listUsersResponse1: null,
  error: null,
});

const before00Setup = async function (testObject: DeleteUserTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);

    testObject.listUsersRequest0 = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
    };
    testObject.listUsersResponse0 = await listUsers(testObject.listUsersRequest0);
    testObject.deleteUserRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
      username: testObject
        .createUserTestObject
        .username,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: DeleteUserTestObject) {
  try {
    testObject.deleteUserResponse = await deleteUser(testObject.deleteUserRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: DeleteUserTestObject) => {
  try {
    const response = await client.deleteUser(testObject.deleteUserRequest);
    testObject.deleteUserResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: DeleteUserTestObject) => {
  try {
    testObject.listUsersRequest1 = testObject.listUsersRequest0;
    testObject.listUsersResponse1 = await listUsers(testObject.listUsersRequest1);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const after00TearDown = async (testObject: DeleteUserTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
  return testObject;
};

const test00 = (testObject: DeleteUserTestObject) => {
  describe('deleteUser', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('deleteUserResponse to deep equal deleteUserRequest', () => {
      expect(testObject.deleteUserResponse).to.deep.equal({
        username: testObject.deleteUserRequest.username,
      });
    });
    it('listUsersResponse0 to include username', () => {
      const o0 = {
        username: testObject
          .createUserTestObject
          .username,
      };
      const o1s = testObject.listUsersResponse0.users;
      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
    it('listUsersResponse1 to not include username', () => {
      const o0 = {
        username: testObject
          .createUserTestObject
          .username,
      };
      const o1s = testObject.listUsersResponse1.users;
      expect(o1s.some(o1 => subsetOf(o0, o1))).false;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
