import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { ListGroupsRequest, ListGroupsResponse } from './Types';
import { cognitoGroupToGroup } from '../../Types';

const listGroups = async (request: ListGroupsRequest): Promise<ListGroupsResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const listGroupsRequest: CognitoIdentityServiceProvider.Types.ListGroupsRequest = {
    UserPoolId: request.customerId,
    Limit: request.limit,
    NextToken: request.paginationToken,
  };

  const cognitoResponse: CognitoIdentityServiceProvider.Types.ListGroupsResponse = await client.listGroups(listGroupsRequest).promise();

  return {
    groups: cognitoResponse.Groups.map(cognitoGroupToGroup),
    paginationToken: cognitoResponse.NextToken,
  };
};

export default listGroups;
