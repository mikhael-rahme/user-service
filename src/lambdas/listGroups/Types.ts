import { Group, CreateGroupTestObject, UserServiceLambdaTestObject } from '../../Types';
export type ListGroupsRequest = {
  customerId: string;
  limit?: number;
  paginationToken?: string;
};
export type ListGroupsResponse = {
  groups: Group[],
  paginationToken?: string,
};

export interface ListGroupsTestObject extends UserServiceLambdaTestObject {
  createGroupTestObject: CreateGroupTestObject;
  listGroupsRequest: ListGroupsRequest;
  listGroupsResponse: ListGroupsResponse;
  error: Error;
}
