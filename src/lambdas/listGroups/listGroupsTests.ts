const { describe, it } = require('mocha');
const expect = require('chai').expect;
import { testUtils } from 'toolkit';
import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { client } from '../../utils';
import { ListGroupsTestObject } from '../../Types';
import listGroups from './';

import * as CreateGroupTests from '../createGroup/createGroupTests';

const { subsetOf } = testUtils;

const createTestObject = (): ListGroupsTestObject => ({
  environment: null,
  createGroupTestObject: CreateGroupTests.createTestObject(),
  listGroupsRequest: null,
  listGroupsResponse: null,
  error: null,
});

const before00Setup = async function (testObject: ListGroupsTestObject) {
  try {
    await CreateGroupTests.before00Setup(testObject.createGroupTestObject);
    await CreateGroupTests.before01Run(testObject.createGroupTestObject);
    await CreateGroupTests.before02CollectResults(testObject.createGroupTestObject);

    testObject.listGroupsRequest = {
      customerId: testObject
        .environment
        .createCustomerResponse.customerId,
    };
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};

const before01Run = async function (testObject: ListGroupsTestObject) {
  try {
    testObject.listGroupsResponse = await listGroups(testObject.listGroupsRequest);
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const run00Integration = async (testObject: ListGroupsTestObject) => {
  try {
    const response = await client.listGroups(testObject.listGroupsRequest);
    testObject.listGroupsResponse = response.data;
  } catch (err) {
    console.log('err.message:', err.message);
    testObject.error = err;
  }
  return testObject;
};
const before02CollectResults = async (testObject: ListGroupsTestObject) => {
  // nada
  return testObject;
};

const after00TearDown = async (testObject: ListGroupsTestObject) => {
  await CreateGroupTests.after00TearDown(testObject.createGroupTestObject);
  return testObject;
};

const test00 = (testObject: ListGroupsTestObject) => {
  describe('listGroups', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('listGroupsResponse to include createGroupResponse object', () => {
      const o0 = { groupName: testObject.createGroupTestObject.groupName };
      const o1s = testObject.listGroupsResponse.groups;

      expect(o1s.some(o1 => subsetOf(o0, o1))).true;
    });
  });
  return testObject;
};

export {
  before00Setup,
  before01Run,
  run00Integration,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
