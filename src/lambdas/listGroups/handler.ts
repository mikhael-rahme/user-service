import {
  Context,
} from 'aws-lambda';
import listGroups from './index';
import { ListGroupsRequest, ListGroupsResponse } from './Types';

const path = require('path');
import {
  LambdaUtils,
  ServiceResult,
  ServiceRequest,
  LambdaLogger,
} from 'toolkit';
const {
  createLambdaHandler,
} = LambdaUtils;

export const handler = createLambdaHandler(
  path.join(__dirname, '../../../serverless.yml'),
  async (
    request: ListGroupsRequest,
    context: Context,
    log: LambdaLogger,
    serviceRequest: ServiceRequest) => {

    const data: ListGroupsResponse = await listGroups(request);
    return new ServiceResult(serviceRequest, { data });
  });
