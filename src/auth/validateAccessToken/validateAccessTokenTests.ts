import { ValidateAccessTokenRequest, ValidateAccessTokenResponse } from './Types';
const { describe, it } = require('mocha');
const expect = require('chai').expect;

import { AuthorizeUserRequest, AuthorizeUserResponse, CreateUserTestObject } from '../../Types';
import validateAccessToken from './';
import authorizeUser from '../authorizeUser';

import * as CreateUserTests from '../../lambdas/createUser/createUserTests';

export type ValidateAccessTokenTestObject = {
  createUserTestObject: CreateUserTestObject;
  authorizeUserRequest: AuthorizeUserRequest;
  authorizeUserResponse: AuthorizeUserResponse;
  validateAccessTokenResponse: ValidateAccessTokenResponse;
  validateAccessTokenRequest: ValidateAccessTokenRequest;
  issuerHost: string;
  error: Error,
};

const createTestObject = (): ValidateAccessTokenTestObject => ({
  createUserTestObject: CreateUserTests.createTestObject(),
  authorizeUserRequest: null,
  authorizeUserResponse: null,
  validateAccessTokenResponse: null,
  validateAccessTokenRequest: null,
  issuerHost: 'https://cognito-idp.us-east-1.amazonaws.com',
  error: null,
});


const before00Setup = async function (testObject: ValidateAccessTokenTestObject) {
  try {
    await CreateUserTests.before00Setup(testObject.createUserTestObject);
    await CreateUserTests.before01Run(testObject.createUserTestObject);
    await CreateUserTests.before02CollectResults(testObject.createUserTestObject);

    const { customerId, clientId } = testObject.createUserTestObject.createCustomerTestObject.createCustomerResponse;
    const { username, password } = testObject.createUserTestObject;


    testObject.authorizeUserRequest = {
      customerId,
      clientId,
      username,
      password,
    };
    testObject.authorizeUserResponse = await authorizeUser(testObject.authorizeUserRequest);
    testObject.validateAccessTokenRequest = {
      accessToken: testObject.authorizeUserResponse.authenticationResponse.accessToken,
      issuer: `${testObject.issuerHost}/${customerId}`,
    };
  } catch (err) {
    console.log(err);
    testObject.error = err;
  }
};

const before01Run = async function (testObject: ValidateAccessTokenTestObject) {
  try {
    testObject.validateAccessTokenResponse = await validateAccessToken(testObject.validateAccessTokenRequest);
  } catch (err) {
    testObject.error = err;
  }
};
const before02CollectResults = async (testObject: ValidateAccessTokenTestObject) => {
  // nada
};

const after00TearDown = async (testObject: ValidateAccessTokenTestObject) => {
  await CreateUserTests.after00TearDown(testObject.createUserTestObject);
};

const test00 = (testObject: ValidateAccessTokenTestObject) => {
  describe('validateAccessToken', () => {
    it('should not have errored', () => {
      expect(testObject.error).to.be.null;
    });
    it('validateAccessTokenResponse should be true', () => {
      expect(testObject.validateAccessTokenResponse).to.be.true;
    });
  });
};

export {
  before00Setup,
  before01Run,
  before02CollectResults,
  test00,
  after00TearDown,
  createTestObject,
};
