export type ValidateAccessTokenRequest = {
  accessToken: string;
  issuer: string;
};

export type ValidateAccessTokenResponse = boolean;
