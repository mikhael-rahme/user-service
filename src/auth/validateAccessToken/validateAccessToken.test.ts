import {
  before00Setup,
  before01Run,
  before02CollectResults,
  after00TearDown,
  test00,
  createTestObject,
} from './validateAccessTokenTests';
const { describe } = require('mocha');

const validateAccessTokenTestObject = createTestObject();

const createTests = (testObject) => {
  describe('validateAccessToken', () => {
    // Setup
    before(async () => before00Setup(testObject));

    // Run
    before(async () => before01Run(testObject));

    // CollectResults
    before(async () => before02CollectResults(testObject));

    // VerifyResults
    test00(testObject);

    // Cleanup
    after(async () => after00TearDown(testObject));
  });
};

createTests(validateAccessTokenTestObject);
