import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { RespondToNewPasswordRequiredRequest, RespondToNewPasswordRequiredResponse } from './Types';
import { cognitoAuthResToAuthRes } from '../../Types';
/*
  respond to challenge NEW_PASSWORD_REQUIRED produced by authorize user.
  May return ADMIN_NO_SRP_AUTH challenge, which require the user to log in with their new username and password.
 */
const respondToNewPasswordRequired = async (request: RespondToNewPasswordRequiredRequest):
  Promise<RespondToNewPasswordRequiredResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const initiateAuthRequest: CognitoIdentityServiceProvider.Types.AdminRespondToAuthChallengeRequest = {
    UserPoolId: request.customerId,
    ClientId: request.clientId,
    ChallengeName: 'NEW_PASSWORD_REQUIRED',
    ChallengeResponses: { USERNAME: request.username, NEW_PASSWORD: request.newPassword },
    Session: request.session,
  };

  const cognitoResponse = await client.adminRespondToAuthChallenge(initiateAuthRequest).promise();

  if (cognitoResponse.ChallengeName !== undefined) {
    if (cognitoResponse.ChallengeName !== 'ADMIN_NO_SRP_AUTH') {
      throw new Error(`Unexpected challengeName in response from Cognito: ${cognitoResponse.ChallengeName}`);
    }

    return {
      challengeName: 'ADMIN_NO_SRP_AUTH',
      session: cognitoResponse.Session,
    };
  }

  return {
    authenticationResponse: {
      accessToken: cognitoResponse.AuthenticationResult.AccessToken,
      refreshToken: cognitoResponse.AuthenticationResult.RefreshToken,
      expiresIn: cognitoResponse.AuthenticationResult.ExpiresIn,
      tokenType: cognitoResponse.AuthenticationResult.TokenType,
      idToken: cognitoResponse.AuthenticationResult.IdToken,
    },
  };
};

export default respondToNewPasswordRequired;
