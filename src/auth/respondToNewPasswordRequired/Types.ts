import { AuthenticationResponse } from '../../Types';
export type ResponseToNewPasswordRequiredChallengeType = 'ADMIN_NO_SRP_AUTH';
export type RespondToNewPasswordRequiredRequest = {
  customerId: string;
  clientId: string;
  username: string;
  newPassword: string;
  session: string;
};
export type RespondToNewPasswordRequiredResponse = {
  authenticationResponse?: AuthenticationResponse;
  challengeName?: ResponseToNewPasswordRequiredChallengeType;
  session?: string;
};
