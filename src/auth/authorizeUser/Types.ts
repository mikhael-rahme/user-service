import { AuthenticationResponse } from '../../Types';

export type AuthorizeUserChallengeType = 'NEW_PASSWORD_REQUIRED';
export type AuthorizeUserRequest = {
  customerId: string;
  clientId: string;
  username: string;
  password: string;
};
export type AuthorizeUserResponse = {
  authenticationResponse?: AuthenticationResponse;
  challengeName?: AuthorizeUserChallengeType;
  session?: string;
};
