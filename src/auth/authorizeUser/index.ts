import { CognitoIdentityServiceProvider } from 'aws-sdk';
import { AuthorizeUserRequest, AuthorizeUserResponse } from './Types';
import { cognitoAuthResToAuthRes } from '../../Types';

/*
  Used to login a user with a given username and password.
  May return a NEW_PASSWORD_REQUIRED challenge, which requires a response with respondToNewPasswordRequired call
    with a username, newPassword, and this session token. quest: is session token required?
 */
const authorizeUser = async (request: AuthorizeUserRequest): Promise<AuthorizeUserResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });

  const initiateAuthRequest: CognitoIdentityServiceProvider.Types.AdminInitiateAuthRequest = {
    UserPoolId: request.customerId,
    ClientId: request.clientId,
    AuthFlow: 'ADMIN_NO_SRP_AUTH',
    AuthParameters: { USERNAME: request.username, PASSWORD: request.password },
  };

  const cognitoResponse = await client.adminInitiateAuth(initiateAuthRequest).promise();

  if (cognitoResponse.ChallengeName !== undefined) {
    if (cognitoResponse.ChallengeName !== 'NEW_PASSWORD_REQUIRED') {
      throw new Error(`Unexpected challengeName in response from Cognito: ${cognitoResponse.ChallengeName}`);
    }

    return {
      challengeName: 'NEW_PASSWORD_REQUIRED',
      session: cognitoResponse.Session,
    };
  }

  return {
    authenticationResponse: {
      accessToken: cognitoResponse.AuthenticationResult.AccessToken,
      refreshToken: cognitoResponse.AuthenticationResult.RefreshToken,
      expiresIn: cognitoResponse.AuthenticationResult.ExpiresIn,
      tokenType: cognitoResponse.AuthenticationResult.TokenType,
      idToken: cognitoResponse.AuthenticationResult.IdToken,
    },
  };
};

export default authorizeUser;
