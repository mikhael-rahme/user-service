const { describe, it } = require('mocha');
const expect = require('chai').expect;

import parseTokens from '../parseTokens';
import {
  before00Setup,
  createTestObject,
} from '../validateAccessToken/validateAccessTokenTests';
import authorizeUser from '../authorizeUser';

import {
  AuthorizeUserResponse,
} from '../../Types';

const validateAccessTokenTestObject = createTestObject();

describe('parseTokens:', () => {
  const accessTokenProperties = [
    'sub',
    'event_id',
    'token_use',
    'scope',
    'iss',
    'exp',
    'iat',
    'jti',
    'client_id',
    'username',
  ];
  let parseTokensResponse = null;
  let authorizeUserResponse: AuthorizeUserResponse = null;
  let error = null;

  // Setup
  before(async () => before00Setup(validateAccessTokenTestObject));
  before(async () => {
    try {
      const { clientId, customerId } = validateAccessTokenTestObject
        .createUserTestObject
        .createCustomerTestObject
        .createCustomerResponse;

      const { username, password } = validateAccessTokenTestObject
        .createUserTestObject;

      authorizeUserResponse = await authorizeUser({
        customerId,
        clientId,
        username,
        password,
      });
      parseTokensResponse = parseTokens(authorizeUserResponse.authenticationResponse.accessToken);
    } catch (err) {
      error = err;
    }
  });
  it('error null', () => {
    expect(error).null;
  });
  it('parseTokensResponse not null', () => {
    expect(parseTokensResponse).not.null;
  });
  it(`token to include properties: ${accessTokenProperties.toString()}`, () => {
    expect(accessTokenProperties.every(x => !!Object.keys(parseTokensResponse).find(y => y === x))).true;
  });

});
