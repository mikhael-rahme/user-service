const parseJwt = (token) => {
  const base64Url = token.split('.')[1];
  const base64 = base64Url.replace('-', '+').replace('_', '/');
  return JSON.parse((new Buffer(base64, 'base64')).toString());
};

export default parseJwt;
