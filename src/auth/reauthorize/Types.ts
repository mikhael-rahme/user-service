import { AuthenticationResponse } from '../../Types';
export type ReauthorizeRequest = {
  customerId: string;
  clientId: string;
  username: string;
  refreshToken: string;
};
export type ReauthorizeResponse = {
  authenticationResponse: AuthenticationResponse;
};
