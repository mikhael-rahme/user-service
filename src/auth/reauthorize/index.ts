import { CognitoIdentityServiceProvider } from 'aws-sdk';
import {ReauthorizeRequest, ReauthorizeResponse } from './Types';
import { cognitoAuthResToAuthRes } from '../../Types';

/*
  Used to refresh an accessToken, if it happens to expire.
 */

const reauthorize = async (request: ReauthorizeRequest): Promise<ReauthorizeResponse> => {

  const client = new CognitoIdentityServiceProvider({
    apiVersion: '2016-04-18',
    region: process.env.region || 'us-east-1',
  });
  const initiateAuthRequest: CognitoIdentityServiceProvider.Types.AdminInitiateAuthRequest = {
    UserPoolId: request.customerId,
    ClientId: request.clientId,
    AuthFlow: 'REFRESH_TOKEN',
    AuthParameters: { USERNAME: request.username, REFRESH_TOKEN: request.refreshToken },
  };

  const cognitoResponse = await client.adminInitiateAuth(initiateAuthRequest).promise();

  if (cognitoResponse.ChallengeName !== undefined) {
    throw new Error('RefreshToken invalid.');
  }

  return {
    authenticationResponse: {
      accessToken: cognitoResponse.AuthenticationResult.AccessToken,
      refreshToken: cognitoResponse.AuthenticationResult.RefreshToken,
      expiresIn: cognitoResponse.AuthenticationResult.ExpiresIn,
      tokenType: cognitoResponse.AuthenticationResult.TokenType,
      idToken: cognitoResponse.AuthenticationResult.IdToken,
    },
  };
};

export default reauthorize;
