import * as addUserToGroupTests from '../lambdas/addUserToGroup/addUserToGroupTests';
import * as createGroupTests from '../lambdas/createGroup/createGroupTests';
import * as createUserTests from '../lambdas/createUser/createUserTests';
import * as deleteGroupTests from '../lambdas/deleteGroup/deleteGroupTests';
import * as deleteUserTests from '../lambdas/deleteUser/deleteUserTests';
import * as getUserTests from '../lambdas/getUser/getUserTests';
import * as listGroupsTests from '../lambdas/listGroups/listGroupsTests';
import * as listUsersTests from '../lambdas/listUsers/listUsersTests';
import * as listUsersInGroupTests from '../lambdas/listUsersInGroup/listUsersInGroupTests';
import * as removeUserFromGroupTests from '../lambdas/removeUserFromGroup/removeUserFromGroupTests';
// import * as resetUserPasswordTests from '../lambdas/resetUserPassword/resetUserPasswordTests';
import * as updateUserTests from '../lambdas/updateUser/updateUserTests';
// import * as forgotPasswordTests from './lambdas/forgotPassword/forgotPasswordTests';
import { testSuite, createTestObject } from './allTests';
import * as changeUserPasswordTests from '../lambdas/changeUserPassword/changeUserPasswordTests';

export const allTests = {
  addUserToGroupTests,
  createGroupTests,
  createUserTests,
  deleteGroupTests,
  deleteUserTests,
  getUserTests,
  listGroupsTests,
  listUsersTests,
  listUsersInGroupTests,
  removeUserFromGroupTests,
  // resetUserPasswordTests,
  updateUserTests,
  // forgotPasswordTests,
  changeUserPasswordTests,
};

export {
  testSuite,
  createTestObject,
}
