import { testSuite, createTestObject, allTests } from './';
const injects: any = {};
Object.keys(allTests).forEach(testKey => injects[testKey] = {
  before01Run: allTests[testKey].run00Integration,
});
const testObject = createTestObject(injects);
testSuite(testObject);
