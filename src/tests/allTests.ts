import * as createCustomerTests from 'customer-service/lib/lambdas/createCustomer/createCustomerTests';
import { CreateCustomerTestObject } from 'customer-service';
import { findDeepAndAssign } from '../utils';
import { allTests } from './';
import { UserServiceTestObject } from './Types';

const before00DefineEnv = async (testObject?: CreateCustomerTestObject): Promise<CreateCustomerTestObject> => {
  return testObject ? testObject : createCustomerTests.createTestObject();
};

const before01EnvSetup = async (testObject: any) => {
  await createCustomerTests.before00Setup(testObject);
  await createCustomerTests.before01Run(testObject);
  await createCustomerTests.before02CollectResults(testObject);
  return testObject;
}

const after00TearDownEnv = async (testObject: any) => {
  try {
    await createCustomerTests.after00TearDown(testObject);
  } catch (err) {
    console.log(`Error during allTests.test.ts teardown for UserPool ${testObject.createCustomerResponse.customerId}:\n\n ${err}`);
  }
  return testObject;
};

/**
 * TODO: Add customEnvironment injection such that graphql service may addon a createUser 
 * @param testInjects An optional parameter, which replaces any portion of a test object with the given value.
 *  -- Usecase: injecting before01Integration into testSuite
 */
const createTestObject = (testInjects: any = {}, customEnvironment?: CreateCustomerTestObject): UserServiceTestObject => {
  const injectOrDefault = (testKey, functionName) => testInjects[testKey] && testInjects[testKey][functionName] ?
    testInjects[testKey][functionName] : allTests[testKey][functionName];
  return {
    environment: null,
    tests: Object.keys(allTests)
    .map(testKey => ({
      before00Setup: injectOrDefault(testKey, 'before00Setup'),
      before01Run: injectOrDefault(testKey, 'before01Run'),
      before02CollectResults: injectOrDefault(testKey, 'before02CollectResults'),
      testObject: injectOrDefault(testKey, 'createTestObject')(),
      after: injectOrDefault(testKey, 'after00TearDown'),
      tests: injectOrDefault(testKey, 'test00'),
      testName: testKey,
    })),
  }
}

/**
 * Works as a framework for all tests within user-service.
 * Creates and removes an environment before and after tests respectively.
 * @param userServiceTestObject 
 */
const testSuite = (userServiceTestObject: UserServiceTestObject) => {
  describe(`user-service:`, () => {
    before(async () => {
      const environment = await before00DefineEnv().then(before01EnvSetup);
      findDeepAndAssign(userServiceTestObject, 'environment', environment);
      console.log(`UserPoolId: ${environment.createCustomerResponse.customerId}`);
    });
    userServiceTestObject.tests.forEach(async (test) => {
      describe(`${test.testName.split('Tests')[0]}:`, () => {
        before(async () => {
          await test.before00Setup(test.testObject)
            .then(test.before01Run)
            .then(test.before02CollectResults);
        });
        test.tests(test.testObject);
        after(async () => {
          try {
            await test.after(test.testObject);
          } catch (err) {
            test.testObject.error = err;
          }
        });
      });
    });
    after(async () => {
      await after00TearDownEnv(userServiceTestObject.environment);
    });
  });
};


export {
  testSuite,
  createTestObject,
}
