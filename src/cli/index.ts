const path = require('path');
const pkg = require('../../package.json');
import { cli } from 'toolkit';
import { argv } from 'yargs';
import getMenuItems from './MenuItems';
const stage = argv.stage || 'dev';
const serviceCLI = new cli.ServiceCli(path.join(__dirname, '../../serverless.yml'), stage);

const run = async () => {
  const userServiceMenuItems = await getMenuItems('cliCaller', stage);
  serviceCLI.run(userServiceMenuItems);
}

run();
