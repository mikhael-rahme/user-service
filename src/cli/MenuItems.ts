import {
  prompt,
  Answers,
  Questions,
} from 'inquirer';

import { 
  ListUsersRequest, 
  ListGroupsRequest, 
  ListUsersInGroupRequest, 
  CreateUserRequest 
} from '../Types';

import {
  cli,
  ServiceResult,
} from 'toolkit';

import * as Table from 'cli-table';

import addUserToGroup from '../lambdas/addUserToGroup';
import createGroup from '../lambdas/createGroup';
import createUser from '../lambdas/createUser';
import deleteGroup from '../lambdas/deleteGroup';
import deleteUser from '../lambdas/deleteUser';
import getUser from '../lambdas/getUser';
import listGroups from '../lambdas/listGroups';
import listUsers from '../lambdas/listUsers';
import listUsersinGroup from '../lambdas/listUsersInGroup';
import removeUserFromGroup from '../lambdas/removeUserFromGroup';
import resetUserPassword from '../lambdas/resetUserPassword';
import updateUser from '../lambdas/updateUser';
import { AddUserToGroupRequest } from '../lambdas/addUserToGroup/Types';
import { CreateGroupRequest } from '../lambdas/createGroup/Types';
import { GetUserRequest } from '../lambdas/getUser/Types';
import { DeleteUserRequest } from '../lambdas/deleteUser/Types';
import { DeleteGroupRequest } from '../lambdas/deleteGroup/Types';
import { RemoveUserFromGroupRequest } from '../lambdas/removeUserFromGroup/Types';
import { ResetUserPasswordRequest } from '../lambdas/resetUserPassword/Types';
import { UpdateUserRequest } from '../lambdas/updateUser/Types';
import createClient, { ListCustomersResponse } from 'customer-service';
import { CLIMenuItems } from 'toolkit/lib/cli/CLIMenuItems';

const getMenuItems = async (caller: string, stage: string): Promise<CLIMenuItems> => {
  const client = createClient(caller, stage);
  
  try{
  const result = await client.listCustomers() as ServiceResult<ListCustomersResponse> ;
  const listCustomersResponse: Questions = await prompt([
    {
      type: 'list',
      name: 'selectedCustomer',
      message: 'Select a Customer',
      choices: result.data.map(customer => customer.customerName),
    },
  ]);

  const selectedCustomerName = listCustomersResponse['selectedCustomer'];
  var selectedCustomer = result.data.find(customer => selectedCustomerName === customer.customerName);
  
  }
  catch(err){
    throw new Error(err);
  }

  return {
    listUsers: async () => {
      try{
        const request: ListUsersRequest = {
          customerId: selectedCustomer.customerId,
        };
        const userList = await listUsers(request);
        const head = ['USERS'];
        const table = new Table ({ head });
        table.push(...userList.users.map(users => [
          users.username,
        ]));
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    }, 

    listGroups: async () => {
      try{
        const request: ListGroupsRequest = {
          customerId: selectedCustomer.customerId,
        };
        const groupList = await listGroups(request);
        const head = ['GROUPS'];
        const table =  new Table ({ head });
        table.push(...groupList.groups.map(groups => [
          groups.groupName,
        ]));
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    listUsersinGroup: async () => {
      try{
        const listUsersInGroupResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'groupName',
            message: 'Input Group Name',
            validate: input => !!input || 'You must give a value',
          },
        ]);
        const request: ListUsersInGroupRequest = {
          customerId: selectedCustomer.customerId,
          groupName: listUsersInGroupResponse['groupName'],
        };
        const usersInGroupList = await listUsersinGroup(request);
        const head = ['GROUP', 'USER'];
        const table = new Table ({ head });
        table.push(...usersInGroupList.users.map(users => [
          request.groupName,
          users.username,
        ]));
        console.log(table.toString());
        /*
        if(user.hasOwnProperty("username")){
          console.log("HAS username");
        }
        */
      }
      catch(err){
        console.log(err);
      }
    },

    getUser: async () => {
      try{
        const getUserResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'username',
            message: 'Input username',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const request: GetUserRequest = {
          customerId: selectedCustomer.customerId,
          username: getUserResponse['username'],
        };
        const user = await getUser(request);
        
        var head = ['USERNAME', 'STATUS', 'ENABLED'];
        const table = new Table ({ head });
        table.push([
          user.username,
          user.status,
          user.enabled,
        ]);
        
        head = ['ATTRIBUTE', 'VALUE'];
        const table2 = new Table ({ head });
        table2.push(...user.userAttributes.map(userAttributes => [
          userAttributes.name, 
          userAttributes.value,
        ]));
        console.log(table.toString());
        console.log(table2.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    addUserToGroup: async () => {
      try{
        const addUserToGroupResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'username',
            message: 'Input username',
            validate: input => !!input || 'You must give a value',
          },
          {
            type: 'input',
            name: 'groupName',
            message: 'Input groupName',
            validate: input => !!input || 'You must give a value',
          },
        ]);
        const request: AddUserToGroupRequest = {
          customerId: selectedCustomer.customerId,
          username: addUserToGroupResponse['username'],
          groupName: addUserToGroupResponse['groupName'],
        };
        const addUser = await addUserToGroup(request);
        const head = ['USER ADDED', 'TO GROUP'];
        const table = new Table ({ head });
        table.push([
          addUser.username,
          addUser.groupName,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    createUser: async () => {
      try{
        const createUserResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'clientID',
            message: 'Input clientID',
            validate: input => !!input || 'You must give a value',
          },
          {
            type: 'input',
            name: 'username',
            message: 'Input username',
            validate: input => !!input || 'You must give a value',
          },
          {
            type: 'input',
            name: 'password',
            message: 'Input password',
            validate: input => !!input || 'You must give a value',
          },
        ]);
        const request: CreateUserRequest = {
          customerId: selectedCustomer.customerId,
          clientId: createUserResponse['clientID'],
          username: createUserResponse['username'],
          password: createUserResponse['password'],
        };
        const createNewUser = await createUser(request);
        const head = ['USER CREATED'];
        const table = new Table ({ head });
        table.push([
          createNewUser.username,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    createGroup: async () => {
      try{
        const createGroupResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'groupName',
            message: 'Input groupName',
            validate: input => !!input || 'You must give a value',
          },
        ]);
        const request: CreateGroupRequest = {
          customerId: selectedCustomer.customerId,
          groupName: createGroupResponse['groupName'],
        };
        const newGroup = await createGroup(request);
        const head = ['GROUP CREATED'];
        const table = new Table ({ head });
        table.push([
          request.groupName,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    deleteUser: async () => {
      try{
        const deleteUserResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'username',
            message: 'Input Username',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const request: DeleteUserRequest = {
          customerId: selectedCustomer.customerId,
          username: deleteUserResponse['username'],
        };
        const delUser = await deleteUser(request);
        const head = ['USER DELETED'];
        const table = new Table ({ head });
        table.push([
          delUser.username,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    deleteGroup: async () => {
      try{
        const deleteGroupResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'groupName',
            message: 'Input Group Name',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const request: DeleteGroupRequest = {
          customerId: selectedCustomer.customerId,
          groupName: deleteGroupResponse['groupName'],
        };
        const delGroup = await deleteGroup(request);
        const head = ['GROUP DELETED'];
        const table = new Table ({ head });
        table.push([
          delGroup.groupName,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    removeUserFromGroup: async () => {
      try{
        const removeUserFromGroupResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'username',
            message: 'Input Username',
            validate: input => !!input || 'You must give a value!',
          },
          {
            type: 'input',
            name: 'groupName',
            message: 'Input Group Name',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const request: RemoveUserFromGroupRequest = {
          customerId: selectedCustomer.customerId,
          username: removeUserFromGroup['username'],
          groupName: removeUserFromGroup['groupName'],
        };
        const delUserFromGroup = await removeUserFromGroup(request);
        const head = ['USER REMOVED', 'FROM GROUP'];
        const table = new Table ({ head });
        table.push([
          delUserFromGroup.username,
          delUserFromGroup.groupName,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    resetUserPassword: async () => {
      try{
        const resetUserPasswordResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'username',
            message: 'Input Username',
            validate: input => !!input || 'You must give a value!',
          },
        ]);
        const request: ResetUserPasswordRequest = {
          customerId: selectedCustomer.customerId,
          username: resetUserPasswordResponse['username'],
        };
        const resetUser = await resetUserPassword(request);
        const head = ['PASSWORD RESET ON USER'];
        const table = new Table ({ head });
        table.push([
          resetUser.username,
        ]);
        console.log(table.toString());
      }
      catch(err){
        console.log(err);
      }
    },

    updateUser: async () => {

      try{
        const getUsernameResponse: Answers = await prompt([
          {
            type: 'input',
            name: 'username',
            message: 'Input Username',
            validate: input => !!input || 'You must give a value!',
          },
        ]);

        const askUpdateEnabledResponse: Questions = await prompt([
          {
            type: 'confirm',
            name: 'askEnabled',
            message: 'Updated Enabled Property?',
          }
        ]);

        if(askUpdateEnabledResponse['askEnabled'] == true){
          var updateEnabledResponse: Answers = await prompt([
            {
              type: 'confirm',
              name: 'updatedEnabled',
              message: 'Enabled?',
            },
          ]);
        }

        const askUpdateGroupsResponse: Questions = await prompt([
          {
            type: 'confirm',
            name: 'askGroups',
            message: 'Update Groups?',
          }
        ]);

        if(askUpdateGroupsResponse['askGroups'] == true){
          var updateGroupsResponse: Answers = await prompt([
            {
              type: 'input',
              name: 'updatedGroups',
              message: 'Input Group',
              validate: input => !!input || 'You must give a value!',
            },
          ]);
        }

        const askUpdateAttResponse: Questions = await prompt([
          {
            type: 'confirm',
            name: 'askGroups',
            message: 'Update Attributes?',
          }
        ]);

        if(askUpdateAttResponse['askGroups'] == true){
          var updateAttResponse: Answers = await prompt([
            {
              type: 'input',
              name: 'updatedAtt',
              message: 'Input Attribute',
              validate: input => !!input || 'You must give a value!',
            },
            {
              type: 'input',
              name: 'updatedValue',
              message: 'Input Attribute Value',
              validate: input => !!input || 'You must give a value!',
            },
          ]);
        }

        const request: UpdateUserRequest = {
          customerId: selectedCustomer.customerId,
          username: getUsernameResponse['username'],
          enabled: updateEnabledResponse['updatedEnabled'],
          groups: updateGroupsResponse['updatedGroups'],
          userAttributes: updateAttResponse['updatedAtt']
        }
        const updatedUser = await updateUser(request);
      }
      catch(err){
        console.log(err);
      }
    }
  };
}

export default getMenuItems;