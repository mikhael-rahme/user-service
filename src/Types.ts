import { CognitoIdentityServiceProvider as Cog } from 'aws-sdk';
import { CreateCustomerTestObject } from 'customer-service';

export * from './lambdas/addUserToGroup/Types';
export * from './lambdas/createGroup/Types';
export * from './lambdas/createUser/Types';
export * from './lambdas/deleteGroup/Types';
export * from './lambdas/deleteUser/Types';
export * from './lambdas/getUser/Types';
export * from './lambdas/listGroups/Types';
export * from './lambdas/listUsers/Types';
export * from './lambdas/listUsersInGroup/Types';
export * from './auth/reauthorize/Types';
export * from './lambdas/removeUserFromGroup/Types';
export * from './lambdas/resetUserPassword/Types';
export * from './lambdas/updateUser/Types';
export * from './lambdas/forgotPassword/Types';
export * from './lambdas/changeUserPassword/Types';

export * from './auth/authorizeUser/Types';
export * from './auth/respondToNewPasswordRequired/Types';

export * from './tests/Types';

export interface UserServiceLambdaTestObject {
  environment: CreateCustomerTestObject;
}

export type UserAttribute = {
  name: string;
  value: string;
};
export type Group = {
  groupName: string;
  description?: string;
};
export type User = {
  username: string;
  userAttributes: UserAttribute[];
  status: Cog.Types.UserStatusType;
  enabled: boolean;
  groups: Group[];
};
export type IterrexUser = {
  username: string;
  customerId: string,
  customerName: string,
  clientId: string,
  groupNames: string[];
};
export type AuthenticationResponse = {
  accessToken: string;
  refreshToken: string;
  expiresIn: number;
  tokenType: string;
  idToken: string;
};

export const cognitoUserAttributeToAttribute = (att: any) => ({ name: att.Name, value: att.Value });

export const cognitoUserToUser = (user) => {

  const response = {
    username: user.Username,
    status: user.UserStatus,
    enabled: user.Enabled,
    groups: user.Groups ? user.Groups.map(cognitoGroupToGroup) : [],
    userAttributes: user.UserAttributes ? user.UserAttributes.map(cognitoUserAttributeToAttribute) : [],
  };

  if (response.userAttributes.length === 0) {
    response.userAttributes = user.Attributes ? user.Attributes.map(cognitoUserAttributeToAttribute) : [];
  }

  return response;
};

export const cognitoAuthResToAuthRes = (auth: Cog.Types.AuthenticationResultType) => ({
  accessToken: auth.AccessToken,
  refreshToken: auth.RefreshToken,
  expiresIn: auth.ExpiresIn,
  tokenType: auth.TokenType,
  idToken: auth.IdToken,
});

export const cognitoGroupToGroup = (grp: Cog.Types.GroupType) => ({
  groupName: grp.GroupName,
  description: grp.Description,
});
