# user-service #

A serverless boilerplate to create services from

### Overview ###

* Leverages Serverless framework and Router toolkit
* Comes with stock cli and parameter management

### How do I get set up? ###
Have an admin fork this repo and name it <service-name>-service, then create your own fork of the newly-forked repo. 
```sh
# clone repo
git clone <repo-url>
cd <service-name>-service
# install packages
yarn
# rename boilerplate name
sed -i 's/test-service/<service-name>-service/g' serverless.yml
sed -i 's/test-service/<service-name>-service/g' package.json
# create env file
touch .env
```
Most of the configuration is set in the serverless.yml, including
* environment variables
* lambda function events, especially http(Api Gateway) events
* Set those environment variable values using the cli
```sh
yarn run cli
go to configuration -> setParameter
```
